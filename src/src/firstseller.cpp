#include "firstseller.h"
#include "ui_firstseller.h"

FirstSeller::FirstSeller(QWidget *parent, DatabaseOperator *pdb) :
    QWidget(parent),
    ui(new Ui::FirstSeller) {
    ui->setupUi(this);
    sf = new SellerForm(this, pdb, true);
    ui->horizontalLayout->addWidget(sf);
    connect(ui->continuePb, &QPushButton::pressed, this, &FirstSeller::addFirstSeller);
    this->setVisible(true);
}

/**
 * @brief FirstSeller::addFirstSeller Saves our first seller to the database.
 * To reload application and start it in invoice mode, app will be restarted.
 */
void FirstSeller::addFirstSeller() {
    sf->addNewSeller();
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

FirstSeller::~FirstSeller() {
    delete ui;
}
