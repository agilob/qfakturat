#ifndef INVOICEVIEW_H
#define INVOICEVIEW_H

#include <QDialog>
#include <QDesktopWidget>
#include "data/Invoice.h"

namespace Ui {
    class InvoiceView;
}

class InvoiceView : public QDialog {
        Q_OBJECT

    public:
        explicit InvoiceView(Invoice *iv, QWidget *parent = 0);
        ~InvoiceView();

    private:
        Ui::InvoiceView *ui;
        void setInvoice(Invoice *inv);
};

#endif // INVOICEVIEW_H
