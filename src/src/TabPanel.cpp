#include "TabPanel.h"

TabPanel::TabPanel(QWidget *parent) : QMainWindow(parent) {
    createDirIfNotExists();
    dbo = new DatabaseOperator();
    tabWidget = new QTabWidget(this);
    this->setWindowIcon(QIcon(":/icons/icon.png"));

    if(!dbo->defaultSellerExists()) {
        new FirstSeller(this, dbo);
    } else {
        invoiceTab = new InvoiceTab(this, dbo);
        settingsTab = new SettingsTab(this, dbo);
        invoiceHistoryTab = new InvoiceHistory(this);
        productshistory = new ProductsHistory(this);
        tabWidget->addTab(invoiceTab, QString::fromUtf8("&Formularz faktury"));
        tabWidget->addTab(productshistory, QString("Historia &sprzedanych produktów"));
        tabWidget->addTab(invoiceHistoryTab, QString::fromUtf8("Historia &faktur"));
        tabWidget->addTab(settingsTab, QString::fromUtf8("&Ustawienia"));
        tabWidget->tabBar()->setExpanding(true);
        tabWidget->setUsesScrollButtons(true);
        tabWidget->setCurrentWidget(invoiceTab);

        setCentralWidget(tabWidget);
    }
    setWindowTitle("QFakturat");
    setMinimumSize(900, 500);

} //TabPanel


void TabPanel::createDirIfNotExists() {
    QString dir = "";
    dir += getenv("HOME");
    dir += "/.config/qfakturat/";

    if (!QDir(dir).exists()) {
        QDir().mkpath(dir);
    }
} // createDirIfNotExist


void TabPanel::invoiceRelay(Invoice *invoice) {
    invoiceTab->setInvoice(invoice);
    tabWidget->setCurrentWidget(invoiceTab);
}


void TabPanel::itemRelay(Item *item) {
    if(item != nullptr) {
        invoiceTab->addItem(item);
    }
}
