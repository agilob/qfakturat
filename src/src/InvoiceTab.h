#ifndef INVOICETAB_H
#define INVOICETAB_H

#include "Generator.h"
#include "data/Invoice.h"
#include "data/Settings.h"
#include "ClientsSelector.h"
#include "DatabaseOperator.h"
#include "invoiceWidgets/paymentDetails.h"
#include "invoiceWidgets/invoiceDetails.h"
#include "invoiceWidgets/itemstable.h"
#include "invoiceWidgets/extrasettings.h"

#include <limits>
#include <QLabel>
#include <QString>
#include <QDateEdit>
#include <QCheckBox>
#include <QGroupBox>
#include <QComboBox>
#include <QLineEdit>
#include <QCompleter>
#include <QMessageBox>
#include <QHeaderView>
#include <QDesktopWidget>
#include <QDoubleSpinBox>

class ItemsTable;

class InvoiceTab : public QWidget {
        Q_OBJECT

    public:
        InvoiceTab(QWidget *parent = 0, DatabaseOperator *pdb = 0);

        void setInvoice(Invoice *invoice);
        void addItem(Item *item);
        void setToPay(double toPay);

    public slots:
        void loadClient();
        void loadClientFromRow(int row, int col);

    private:
        PaymentDetails *p_d;
        InvoiceDetails *i_d;
        ExtraSettings *extraSettings;
        ItemsTable *table;
        ClientsSelector *clientsLoader;
        Invoice *invoice;
        DatabaseOperator *dbO;

        QComboBox *invoiceTypes;

        QList<Client *> *clients;

        QStringList methodList;

        QVBoxLayout *leftLayout;

        QHBoxLayout *hbox;

        QGroupBox *buttonsBox;
        QGridLayout *buttonsLayout;

        QPushButton *loadClientsButton,
                    *saveClientButton,
                    *addRowButton,
                    *acceptInvoiceButton,
                    *removeRowButton,
                    *additionalSettings;

        QStringList vats,
                    measures;

        QLabel *locationLa, ///< a label to specify town
               *invoiceLa, ///< a label to specify invoiceID
               *nameLa, ///< a label for a name of company
               *nameSurnameLa, ///< a label for name and surname
               *addressLa, ///< a label for city
               *streetLa, ///< a label for name of a street
               *nipLa; ///< a label for NIP

        QLineEdit *nameLE, ///< a field to type client's company name
                  *nameSurnameLE, ///< a field to type client's name
                  *addressLE, ///< a field to type client's address
                  *streetLE, ///< a field to type client's street
                  *nipLE; ///< a field to type client's NIP

        QGroupBox *leftBox, ///< includes all smaller boxes; main left box
                  *invoiceTypesBox, ///< a box with checkboxes to choose copy and/or original version on an invoice
                  *datesBox, ///< a box for dates, town, invoice ID
                  *buyerBox, ///< a box at right, has information about buyer
                  *centralBox, ///< a box with other boxes at right
                  *itemsBox; ///< a box for sold items

        QGridLayout *centralLayout, ///< positioning objects in centralBox
                    *wholeLayout, ///< positioning objects in whole invoice-UI
                    *datesLayout, ///< positioning objects in datesBox
                    *buyerLayout, ///< positioning objects in buyer box
                    *payingLayout, ///< positioning objects in paying box
                    *itemsLayout; ///< layout for sold items

        void setClient(Client *client);
        QString getLastInvoiceID(QString invoiceId);
        void addAutocompleters();
        double sumUpToPay();
        bool isSumValid();

    private slots:
        void findClientByCompany(QString company);
        void findClientByName(QString company);
        void findClientByNip(QString nip);
        void findClientByAddress(QString address);
        void findClientByStreet(QString street);
        void onAdditionalSettings();

        void saveClient();

        /**
         * @brief If NIP is incorrect, font in nipLe is set to red, it it's correct,
         * font is black and NIP is formated to be easy to read.
         */
        void validateNIP();

        /**
         * @brief Fills a structure to generate a PDF from it.
         */
        void acceptInvoice();

        /*
         * @brief Opens new window with clients to load to the form
         */
        void openClientsWindow();

        Client *getClient();

        /**
         * @brief Sum ups all netto values for all products in table.
         * @return Total netto price
         */
        double sumUpToPayNetto();

        /**
         * @brief Method loads from SQLite database first seller who is set to be default.
         * Because there is a bug and many seller can be default, the first one will be selected.
         */
        void loadDefaultSeller();
};

#endif  /* INVOICE_H */
