#include "InvoiceTab.h"
#include "ClientsSelector.h"

ClientsSelector::ClientsSelector(InvoiceTab *parent, QList<Client *> *clients) : QDialog() {
    columns << "Nazwa" << QString::fromUtf8("Nazwisko i imię")
            << "Miasto" << "Ulica" << "NIP";
    table = new QTableWidget(this);
    table->setColumnCount(columns.size());
    table->setHorizontalHeaderLabels(columns);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    layout = new QGridLayout(this);
    layout->addWidget(table, 0, 0);
    table->setRowCount(clients->size());

    for (int j = 0; j < clients->size(); j++) {
        table->setItem(j, 0, new QTableWidgetItem(clients->at(j)->company));
        table->setItem(j, 1, new QTableWidgetItem(clients->at(j)->name));
        table->setItem(j, 2, new QTableWidgetItem(clients->at(j)->city));
        table->setItem(j, 3, new QTableWidgetItem(clients->at(j)->street));
        table->setItem(j, 4, new QTableWidgetItem(clients->at(j)->NIP));
    }

    for(int i = 0; i < table->columnCount(); i++) {
        for(int j = 0; j < table->rowCount(); j++) {
            table->item(j, i)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        }
    }

    accept = new QPushButton(QString::fromUtf8("Wczytaj\nwybranego\nklienta"));
    layout->addWidget(accept, 0, 1);
    connect(accept, &QPushButton::clicked, parent, &InvoiceTab::loadClient);
    connect(table, &QTableWidget::cellDoubleClicked, parent, &InvoiceTab::loadClientFromRow);

    this->setLayout(layout);
    this->resize(table->width() + 180, table->height() / 3);
    this->setLayout(layout);
    this->show();
    //move to the middle of the main screen
    this->setGeometry(QStyle::alignedRect(
                          Qt::LeftToRight,
                          Qt::AlignCenter,
                          this->size(),
                          QApplication::desktop()->availableGeometry()));
}
