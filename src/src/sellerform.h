#ifndef SELLERFORM_H
#define SELLERFORM_H

#include "Validators.h"
#include "data/Seller.h"
#include "DatabaseOperator.h"

#include <QWidget>
#include <QPalette>
#include <QComboBox>
#include <QPushButton>

namespace Ui {
    class SellerForm;
}

class SellerForm : public QWidget {
        Q_OBJECT

    public:
        SellerForm(QWidget *parent, DatabaseOperator *pdb);
        explicit SellerForm(QWidget *parent, DatabaseOperator *pdb, bool defaultSelected);
        ~SellerForm();

        Ui::SellerForm *ui;
        void addNewSeller();


    public slots:
        void validateNIP();


    private:
        DatabaseOperator *dbo;
        void setSeller(Seller *seller);
        void loadSellerInfo();
        void loadIDs();
        void setUp();

        QPushButton *addSeller,
                    *removeSeller,
                    *saveChanges;
        QComboBox *ids;

    private slots:
        void saveData();
        void deleteSeller();
        void changeSeller(int nope);

};

#endif // SELLERFORM_H
