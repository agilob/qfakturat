#ifndef VALIDATORS_H
#define VALIDATORS_H

#include <QString>
#include <QRegExp>

class Validators {

    public:
        Validators();
        virtual ~Validators();

        /**
         * @brief Method returns true if NIP is valid, false if it's not.
         *
         * Algorithm is based on a structe of a NIP number.
         */
        static bool validateNIP(QString nip);

        /**
         * @brief returns a formated string which is the same given NIP number, but
         * formated to be easier to read by people on the invoice or in the app.
         * The new format of this string is XXX-XXX-XX-XX
         */
        static QString formatNIP(QString nip);

};

#endif // VALIDATORS_H
