#include "databasesettings.h"
#include "ui_databasesettings.h"

#include "data/style.h"

DatabaseSettings::DatabaseSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DatabaseSettings) {
    ui->setupUi(this);
    setSettings(); //set values in Settings instance
    connect(ui->save, &QPushButton::clicked, this, &DatabaseSettings::saveSettings);
    connect(ui->useSystemTheme, &QCheckBox::clicked, this, &DatabaseSettings::onSystemThemeEnabled);
    connect(ui->theme, &QComboBox::currentTextChanged, this, &DatabaseSettings::onSystemThemeChanged);
}


void DatabaseSettings::setSettings() {
    ui->taxes->setText(Settings::getInstance().getTaxes());
    ui->measures->setText(Settings::getInstance().getMeasures());
    ui->defaultVat->setText(Settings::getInstance().getDefaultVAT());
    ui->invoiceTypes->setText(Settings::getInstance().getInvoiceTypes());
    ui->paymentMethod->setText(Settings::getInstance().getPaymentMethods());
    ui->currencies->setText(Settings::getInstance().getCurrencies());
    ui->daysToPay->setValue(Settings::getInstance().getDaysForPayment());
    ui->formatNip->setChecked(Settings::getInstance().getFormatNip());
    // kinda hack, watch out for next line, `NOT getIsToPayDefault`
    ui->toPay->setChecked(Settings::getInstance().getIsToPayDefault());
    ui->alreadyPaid->setChecked(!Settings::getInstance().getIsToPayDefault());
    //
    ui->autoInvoiceNumber->setChecked(Settings::getInstance().getAutoInvoiceNumber());
    ui->statisticData->setChecked(Settings::getInstance().getStatisticDataEnabled());
    // themes
    ui->useSystemTheme->setChecked(Settings::getInstance().getUseSystemTheme());
    //
    ui->theme->setEnabled(!Settings::getInstance().getUseSystemTheme());
    ui->theme->addItems(Settings::getInstance().getThemes());
    ui->theme->setCurrentIndex(Settings::getInstance().getTheme());
} //setSettings


void DatabaseSettings::saveSettings() {
    Settings::getInstance().setPaymentMethods(ui->paymentMethod->text());
    Settings::getInstance().setIsToPayDefault(ui->toPay->isChecked());
    Settings::getInstance().setTaxes(ui->taxes->text());
    Settings::getInstance().setMeasures(ui->measures->text());
    Settings::getInstance().setInvoiceTypes(ui->invoiceTypes->text());
    Settings::getInstance().setDefaultVAT(ui->defaultVat->text());
    Settings::getInstance().setCurrencies(ui->currencies->text());
    Settings::getInstance().setDaysForPayment(ui->daysToPay->value());
    Settings::getInstance().setFormatNip(ui->formatNip->isChecked());
    Settings::getInstance().setAutoInvoiceNumber(ui->autoInvoiceNumber->isChecked());
    Settings::getInstance().setStatisticDataEnabled(ui->statisticData->isChecked());
    Settings::getInstance().setUseSystemTheme(ui->useSystemTheme->isChecked());
    Settings::getInstance().setTheme(ui->theme->currentIndex());
    Settings::getInstance().save();
} //saveDatabase


void DatabaseSettings::onSystemThemeEnabled() {
    ui->theme->setEnabled(!ui->useSystemTheme->isChecked());
    saveSettings();
}


void DatabaseSettings::onSystemThemeChanged(QString theme) {
    dynamic_cast<QApplication *>(QApplication::instance())->setStyleSheet(Style::getStylesheet(":/css/" +
            theme + ".css"));
    saveSettings();
}


DatabaseSettings::~DatabaseSettings() {
    delete ui;
}

