#include "productshistory.h"
#include "ui_productshistory.h"

#define BUTTON 0
#define NAME 1
#define MEASURE 2
#define PRICE 3
#define QUANTITY 4
#define VAT 5

ProductsHistory::ProductsHistory(TabPanel *myParent) : ui(new Ui::ProductsHistory) {
    ui->setupUi(this);
    ui->afterDate->hide();
    ui->beforeDate->hide();
    ui->soldAfterLabel->hide();
    ui->soldBeforeLabel->hide();
    ui->afterDate->setFirstDayOfWeek(Qt::Monday);
    ui->beforeDate->setFirstDayOfWeek(Qt::Monday);
    parent = myParent;

    QStringList list;
    list << "" << "Nazwa" << "Miara" << "Cena jedn." << "Ilość" << "VAT";
    QDate today = ui->afterDate->selectedDate();
    today.setDate(today.year() - 1, today.month(), 1);
    ui->afterDate->setSelectedDate(today);
    ui->beforeDate->setSelectedDate(QDate::currentDate());
    ui->productsTable->setColumnCount(list.size());
    ui->productsTable->setHorizontalHeaderLabels(list);
    ui->productsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->productsTable->setContextMenuPolicy(Qt::CustomContextMenu);
    buttons = new QList<QPushButton *>();
    items = parent->dbo->loadItems();
    this->maxPrice = getMaxPrice();
    this->minPrice = 0;
    ui->maxPrice->setValue(maxPrice);
    ui->vatBox->addItems(getListOfVats());

    for(int row = 0; row < items->size(); row++) {
        QPushButton *button = new QPushButton(tr("Wczytaj", "load invoice"));
        buttons->append(button);
        connect(button, &QPushButton::clicked, this, &ProductsHistory::locateButtonAndSendItem);
        ui->productsTable->insertRow(row);
        ui->productsTable->setCellWidget(row, BUTTON, button);
        ui->productsTable->setItem(row, NAME,
                                   new QTableWidgetItem(items->at(row)->getName()));
        ui->productsTable->setItem(row, MEASURE,
                                   new QTableWidgetItem(items->at(row)->getMeasure()));
        ui->productsTable->setItem(row, PRICE,
                                   new QTableWidgetItem(QString::number(items->at(row)->getSinglePrice(), 'f', 2)));
        ui->productsTable->setItem(row, QUANTITY,
                                   new QTableWidgetItem(QString::number(items->at(row)->getQuantity(), 'f', 2)));
        ui->productsTable->setItem(row, VAT,
                                   new QTableWidgetItem(QString::number(items->at(row)->getVat())));
    }

    for(int i = 0; i < ui->productsTable->rowCount(); i++) {
        //button is in the first column, no text alignment
        for(int j = 1; j < list.size(); j++) {
            ui->productsTable->item(i, j)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            ui->productsTable->item(i, j)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            ui->productsTable->item(i, j)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            ui->productsTable->item(i, j)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        }
    }

    connect(ui->productName, &QLineEdit::textEdited, this, &ProductsHistory::onNameChanged);
    connect(ui->minPrice, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &ProductsHistory::onMinPriceChanged);
    connect(ui->maxPrice, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &ProductsHistory::onMaxPriceChanged);
    connect(ui->vatBox, &QComboBox::currentTextChanged, this, &ProductsHistory::onVatChanged);
}


void ProductsHistory::applyFilters() {
    for(int i = 0; i < ui->productsTable->rowCount(); i++) {
        if(!ui->productsTable->item(i, NAME)->text().contains(this->name)
           || ui->productsTable->item(i, PRICE)->text().toDouble() < this->minPrice
           || ui->productsTable->item(i, PRICE)->text().toDouble() > this->maxPrice
           || (ui->vatBox->currentIndex() != 0 && ui->productsTable->item(i, VAT)->text() != this->vat)
          ) {
            ui->productsTable->hideRow(i);
        } else {
            ui->productsTable->showRow(i);
        }
    }
}

QStringList ProductsHistory::getListOfVats() {
    QStringList vats;
    vats << tr("Wszystkie", "all VAT levels");

    foreach (Item *i, *items) {
        vats << QString::number(i->getVat());
    }

    vats.removeDuplicates();
    return vats;
}

/**
 * @brief ProductsHistory::filterTable Hides products that don't match filter.
 *        Otherwise shows them, in case they were hidden before.
 * @param filter String which is checked in contains() for products name.
 */
void ProductsHistory::onNameChanged(QString filter) {
    this->name = filter;
    applyFilters();
}

void ProductsHistory::onMinPriceChanged(double price) {
    this->minPrice = price;
    applyFilters();
}

void ProductsHistory::onMaxPriceChanged(double price) {
    this->maxPrice = price;
    applyFilters();
}

void ProductsHistory::onVatChanged(QString vat) {
    this->vat = vat;
    applyFilters();
}

double ProductsHistory::getMaxPrice() {
    double max = 0;

    for(int i = 0; i<items->size(); i++) {
        if(max < items->at(i)->getSinglePrice()) {
            max = items->at(i)->getSinglePrice();
        }
    }

    return max;
}

void ProductsHistory::locateButtonAndSendItem() {
    for (int i = 0; i < buttons->size(); i++) {
        if(buttons->at(i) == QObject::sender()) {
            parent->itemRelay(items->at(i));
        }
    }
}

void ProductsHistory::changeEvent(QEvent *e) {
    QWidget::changeEvent(e);

    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;

        default:
            break;
    }
}

ProductsHistory::~ProductsHistory() {
    delete ui;
}
