#ifndef GENERATOR_H
#define GENERATOR_H

#include "data/Item.h"
#include "Validators.h"
#include "data/Invoice.h"
#include "data/Settings.h"

#include <QPen>
#include <QDir>
#include <QUrl>
#include <QRect>
#include <QDate>
#include <QFont>
#include <QDebug>
#include <qmath.h>
#include <QWidget>
#include <QPrinter>
#include <QPainter>
#include <QPushButton>
#include <QFontDatabase>
#include <QDesktopServices>

/**
 * @brief Generates PDF file.
 */
class Generator : public QWidget {
        Q_OBJECT

    public:
        Generator();
        Generator(Invoice *invoice);

        void generatePdf(Invoice *invoice);

    private:

        void init();

        QPrinter printer; ///< The QPrinter class is a paint device that paints on a printer
        QPainter painter; ///< performs painting
        QPoint *point; ///< to place things on pdf
        QRect *rec; ///< default rectangle object used to draw at invoice

        int x,
            y, //to work with location
            position; //a place where we start writing about customer

        QFont font,
              originalFont;

        QString fileName;

        Invoice *invoice; ///< Provided invoice will be stored here for a life of this class.

        void drawTop();

        /**
         * @brief Draws a rectangle with details about a seller
         *
         * This rectangle must be the same high as a retangle for client.
         */
        void drawSeller();

        void drawClient();

        void drawDates();

        void drawInvoiceNumber();

        void drawLine();

        void drawTable();

        void drawSideText();

        void drawSellerInfo();

        /**
         * @brief Draws dots to make dedicated space for signatures.
         */
        void drawDots();

        void drawBottomText();

        QString slownie(int liczba);

        int rozklad_liczby(unsigned short rozklad[4], unsigned long liczba);


        /**
         * @brief Gives length of the longest string in the list
         *
         * The longest string from string list. We need this method to know how wide
         * a rectangle is.
         * @param st List to find the longest name.
         * @return Length of the longest string.
         */
        int longestName(QStringList st);

        /**
         * @brief Draws table with products.
         *
         * Method manages and generates table for selected products.
         * Tables contains columns like: product name, price, taxes, measurement,
         * quantity, price net and with taxes.
         * @param item list of items
         * @param painter painter from generatePdf
         * @param printer printer from generatePdf
         * @param point height of document, where the table will have top
         */
        int generateTable(QList<Item> item, QPainter &painter, QPrinter &printer, QPoint point);

        /**
         * @brief Method generates file name and directory to store it.
         * @param orginalOrCopy If original - true if copy - false
         * @return Absolute path to the file.
         */
        QString generateFileName();


        /**
         * @brief Open newly generated file by system standard application.
         * @param filePath File to open.
         */
        void openFile(QString filePath);

};


#endif  /* GENERATOR_H */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;
