#include "Generator.h"

Generator::Generator() {
    init();
    font.setPixelSize(11);
}

Generator::Generator(Invoice *inv) {
    this->invoice = inv;
    init();
    font.setPixelSize(11);
    generatePdf(inv);
}


// fonts are built-in in resources
void Generator::generatePdf(Invoice *inv) {
    if(this->invoice == nullptr)
        this->invoice = inv;

    font.setFamily("DejaVuSans");
    drawDates();
    drawTop();
    font.setFamily("FreeMono");
    drawInvoiceNumber();
    drawLine();
    font.setFamily("FreeMono");
    drawTable();
    drawSellerInfo();
    drawSideText();
    drawBottomText();
    font.setFamily("DejaVuSans");
    drawDots();
    painter.end();
    openFile(fileName);
} // generatePdf


void Generator::drawDots() {
    point->setX(point->x() + 50);
    point->setY(printer.height() - 25);
    //
    painter.drawText(*point,
                     QString("..................................") +
                     QString(".................................."));
    point->setX((printer.width() / 2) + 55);
    painter.drawText(*point,
                     QString("..................................") +
                     QString(".................................."));
} // drawDots


void Generator::drawSellerInfo() {
    painter.setPen(Qt::black);
    rec->setRect(50, point->y() + 20,
                 400, 220);
    QString sellerInfo = invoice->seller->name + "\n";
    !invoice->seller->email.isNull() &&!invoice->seller->email.isEmpty() ?
    sellerInfo += "e-mail: " + invoice->seller->email + "\n" :"";
    !invoice->seller->www.isNull() &&!invoice->seller->www.isEmpty() ?
    sellerInfo += "www: " + invoice->seller->www + "\n" : "";
    !invoice->seller->phone.isNull() &&!invoice->seller->phone.isEmpty() ?
    sellerInfo += "tel.: " + invoice->seller->phone + "\n" :"";
    !invoice->seller->bankAccount.isNull() &&!invoice->seller->bankAccount.isEmpty() ?
    sellerInfo += invoice->seller->bankAccount + "\n" :"";
    sellerInfo += "\n";
    painter.drawText(*rec,
                     sellerInfo +
                     QString::fromUtf8("Do zapłaty: ") +
                     QString::number(invoice->toPay, 'f', 2) + invoice->currency + " \n" +
                     QString::fromUtf8("Zapłacono: ") +
                     QString::number(invoice->paid, 'f', 2) + invoice->currency + " \n" +
                     QString::fromUtf8("Termin zapłaty: ") +
                     invoice->deadDate.toString("dd.MM.yyyy") + "\n" +
                     QString::fromUtf8("Metoda zapłaty: ") +
                     invoice->payingMethod
                    );
} // drawSellerInfo


void Generator::drawBottomText() {
    rec->setRect(0, printer.height() - 55, printer.width(), 100);
    painter.drawText(*rec, Qt::AlignCenter, invoice->seller->bottomText);
} // drawBottomText


void Generator::drawTable() {
    // This method is just one huge WTF
    // http://images.cryhavok.org/d/2471-1/WTFs+per+Minute.jpg
    // Skip reading this and do some computer-related job with computers.
    QPoint rightSide,
           leftSide;
    int leftX,
        levelY;
    /*
     Positioning table with products
     */
    levelY = point->y() + 45;
    leftX = 30;
    leftSide.setX(leftX);
    leftSide.setY(levelY);
    rightSide.setX(printer.width());
    rightSide.setY(levelY);
    QRect name, singlePrice, type, quantity, netto, vat, tax, brutto; //rectangles for cells
    levelY = point->y() + 20;
    leftSide.setY(levelY);
    rightSide.setY(levelY);
    /*************************************************************************/
    /***************** End of positioning table with products *****************/
    /*************************************************************************/
    int x = 50,
        y = leftSide.y(),
        w = 270,
        h = 28;
    /*
     * This is the WTF I was talking about.
     * This part I've done once.
     * I know what it is about.
     * Don't even try to understand...
     * Seriously it's a waste of time.
     */
    int width = 0; //X point where a column starts
    int nameW = 270; //width of a column
    int typeW = 40;
    int quantityW = 55;
    int singlePriceW = 55;
    int nettoW = 95;
    int vatW = 20;
    int taxW  = 75;
    int bruttoW = 95;
    //70+30+95+40+40+40+270=585 = max width
    name.setRect(x, y, w, h);
    width += nameW;
    type.setRect(x + width, y, typeW, h);
    width += typeW;
    quantity.setRect(x + width, y, quantityW, h);
    width += quantityW;
    singlePrice.setRect(x + width, y, singlePriceW, h);
    width += singlePriceW;
    netto.setRect(x + width, y, nettoW, h);
    width += nettoW;
    vat.setRect(x + width, y, vatW, h);
    width += vatW;
    tax.setRect(x + width, y, taxW, h);
    width += taxW;
    brutto.setRect(x + width, y, bruttoW, h);
    painter.drawRect(name);
    painter.drawRect(type);
    painter.drawRect(quantity);
    painter.drawRect(singlePrice);
    painter.drawRect(netto);
    painter.drawRect(vat);
    painter.drawRect(tax);
    painter.drawRect(brutto);
    painter.fillRect(name, Qt::lightGray);
    painter.fillRect(type, Qt::lightGray);
    painter.fillRect(quantity, Qt::lightGray);
    painter.fillRect(singlePrice, Qt::lightGray);
    painter.fillRect(netto, Qt::lightGray);
    painter.fillRect(vat, Qt::lightGray);
    painter.fillRect(tax, Qt::lightGray);
    painter.fillRect(brutto, Qt::lightGray);
    painter.drawText(name, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("Nazwa produktu"));
    painter.drawText(type, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("Miara"));
    painter.drawText(quantity, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("Ilość"));
    painter.drawText(singlePrice, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("CJ"));
    painter.drawText(netto, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("Netto"));
    painter.drawText(tax, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("Podatek"));
    painter.drawText(vat, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("VAT"));
    painter.drawText(brutto, Qt::TextWordWrap | Qt::AlignCenter,
                     QString::fromUtf8("Brutto"));
    y = y + h; //row a bit lower than before...
    h = 35;

    for (int i = 0; i < invoice->items->size(); i++) {
        if (invoice->items->at(i)->getName().size() > 40) { //if items name if longer then 40 chars
            h = 30 + (invoice->items->at(i)->getName().size() / 25) * 10;
        } else {
            h = 25;
        }

        width = 0;
        name.setRect(x, y, w, h);
        width += nameW;
        type.setRect(x + width, y, typeW, h);
        width += typeW;
        quantity.setRect(x + width, y, quantityW, h);
        width += quantityW;
        singlePrice.setRect(x + width, y, singlePriceW, h);
        width += singlePriceW;
        netto.setRect(x + width, y, nettoW, h);
        width += nettoW;
        vat.setRect(x + width, y, vatW, h);
        width += vatW;
        tax.setRect(x + width, y, taxW, h);
        width += taxW;
        brutto.setRect(x + width, y, bruttoW, h);
        painter.drawText(name, Qt::TextWordWrap | Qt::AlignLeft | Qt::AlignVCenter,
                         QString(invoice->items->at(i)->getName()));
        painter.drawText(type , Qt::AlignCenter,
                         invoice->items->at(i)->getMeasure());
        painter.drawText(quantity, Qt::AlignCenter,
                         QString::number(invoice->items->at(i)->getQuantity(), 'f', 2));
        painter.drawText(singlePrice, Qt::AlignCenter,
                         QString::number(invoice->items->at(i)->getSinglePrice(), 'f', 2));
        painter.drawText(netto , Qt::AlignCenter,
                         QString::number(invoice->items->at(i)->getNetto(), 'f', 2));
        painter.drawText(vat , Qt::AlignCenter,
                         QString::number(invoice->items->at(i)->getVat()));
        painter.drawText(tax, Qt::AlignCenter,
                         QString::number(invoice->items->at(i)->getGross() - invoice->items->at(
                                             i)->getNetto(), 'f', 2));
        painter.drawText(brutto, Qt::AlignCenter,
                         QString::number(invoice->items->at(i)->getGross(), 'f', 2));
        y = y + h; //row a bit lower than before...
    } // printing table with items

    h = 20;

    QRect sum;
    y = y + 20; //row a bit lower than before...
    sum.setRect(x += 500, y, 50, h);
    netto.setRect(x, y, 50, h);
    vat.setRect(x, y, 50, h);
    brutto.setRect(x, y, 50, h);
    painter.drawText(sum, Qt::AlignRight, "Suma");
    name.setRect(x, y += h, 50, h);
    painter.drawText(name, Qt::AlignRight, QString::fromUtf8("Netto:"));
    netto.setRect(x + 50, y, 125, h);
    painter.drawText(netto, Qt::AlignRight, QString::number(invoice->netto, 'f', 2) + " " + invoice->currency);
    name.setRect(x, y += h, 50, h);
    painter.drawText(name, Qt::AlignRight, QString::fromUtf8("VAT:"));
    vat.setRect(x + 50, y, 125, h);
    painter.drawText(vat, Qt::AlignRight, QString::number(invoice->totalVat, 'f', 2) + " " + invoice->currency);
    name.setRect(x, y += h, 50, h);
    painter.drawText(name, Qt::AlignRight, QString::fromUtf8("Brutto:"));
    brutto.setRect(x + 50, y, 125, h);
    painter.drawText(brutto, Qt::AlignRight, QString::number(invoice->brutto, 'f', 2) + " " + invoice->currency);
    name.setRect(x - 100, y += h, 300, 150);
    point->setY(y);
}


void Generator::drawLine() {
    QPen *pen = new QPen(Qt::black, 2, Qt::DashLine),
    *pen0 = new QPen();
    point->setY(point->y() + 20);
    int x = 55;
    painter.setPen(*pen);
    painter.drawLine(QPoint(x, point->y()),
                     QPoint(printer.width() - x, point->y()));
    painter.setPen(*pen0);
}


void Generator::drawSeller() {
    rec = new QRect(50, point->y(),
                    printer.width() / 2 - 50, 115);
    painter.setFont(font);
    painter.drawRect(*rec);
    painter.drawText(*rec, QString::fromUtf8("   Sprzedający:"));
    painter.setFont(originalFont);
    QString nip = invoice->seller->NIP;

    if(Settings::getInstance().getFormatNip() == true) {
        nip = Validators::formatNIP(nip);
    }

    painter.drawText(*rec,
                     "\n\n   " + invoice->seller->company + "\n   " +
                     invoice->seller->name + "\n   " +
                     invoice->seller->postcode + " " + invoice->seller->city + "\n   " +
                     invoice->seller->street + "\n   " +
                     QString::fromUtf8("NIP: ") +
                     nip
                    );
} // drawSeller


void Generator::drawClient() {
    rec = new QRect(printer.width() / 2, point->y(),
                    printer.width() / 2 - 50, 115);
    painter.drawRect(*rec);
    painter.setFont(font);
    painter.drawText(*rec, QString::fromUtf8("   Nabywca:"));
    painter.setFont(originalFont);
    QString nip = invoice->client->NIP;

    if(Settings::getInstance().getFormatNip() == true) {
        nip = Validators::formatNIP(nip);
    }

    painter.drawText(*rec,
                     "\n\n   " + invoice->client->company + "\n   " +
                     invoice->client->name + "\n   " +
                     invoice->client->city + "\n   " +
                     invoice->client->street + "\n   " +
                     QString::fromUtf8("NIP: ") +
                     nip
                    );
} // drawClient

void Generator::drawInvoiceNumber() {
    QString invoiceNumber = invoice->type + " " + invoice->invoiceNumber;
    font.setPixelSize(21);
    painter.setFont(font);
    point->setX(printer.width() / 2 - invoiceNumber.length() * 7);
    point->setY(point->y() + 150);
    painter.drawText(*point, invoiceNumber);
    painter.setFont(originalFont);
} // drawInvoiceNumber


void Generator::drawTop() {
    point->setY(point->y() + 15);
    drawSeller();
    drawClient();
} // drawTop


void Generator::drawDates() {
    point->setX(printer.width() / 2);
    point->setY(point->y() + 15);
    painter.drawText(*point,
                     "Data i miejsce sprzedaży: " + invoice->location + ", " +
                     invoice->sellingDate.toString("dd.MM.yyyy"));
    point->setY(point->y() + 15);
    painter.drawText(*point,
                     "Data wystawienia: " + invoice->creatingDate.toString("dd.MM.yyyy"));
} // drawDates


void Generator::drawSideText() {
    point->setX(0);
    point->setY(0);
    font.setPixelSize(27);
    painter.setFont(font);
    painter.rotate(90);
    painter.setPen(Qt::gray);
    painter.drawText(*point, invoice->seller->rightText);
    painter.rotate(-90);
} // drawSideText


void Generator::init() {
    QFontDatabase fontDatabase;
    fontDatabase.addApplicationFont(":/fonts/FreeMono.ttf");
    fontDatabase.addApplicationFont(":/fonts/SLC_.ttf");
    fontDatabase.addApplicationFont(":/fonts/DejaVuSans.ttf");
    point = new QPoint();
    printer.setOutputFormat(QPrinter::PdfFormat);
    fileName = generateFileName();
    printer.setOutputFileName(fileName);
    printer.setCreator("QFakturat");
    painter.begin(&printer);
} // init


QString Generator::generateFileName() {
    QString fullPath = "";
    QString configDir = Settings::getSettingsDirPath();
    QDateTime currentTime = QDateTime::currentDateTime();
    QString directory = configDir + "/archiwum/" + currentTime.toString("yyyy_MM") + "/";
    QDir *dir = new QDir(directory);
    dir->toNativeSeparators(directory);

    if (!dir->exists()) {
        dir->mkpath(directory);
    }

    fullPath = dir->path() + QDir::separator() +
               currentTime.toString(QString("dd_hh_MM_ss_" + invoice->client->NIP)) + ".pdf";
    return fullPath;
} // generateFileName


void Generator::openFile(QString filePath) {
    QDesktopServices::openUrl(QUrl("file://" + filePath));
} // openFile

