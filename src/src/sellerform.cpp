#include "sellerform.h"
#include "ui_sellerform.h"


SellerForm::SellerForm(QWidget *parent, DatabaseOperator *pdb, bool defaultSelected) :
    QWidget(parent),
    ui(new Ui::SellerForm) {
    Q_UNUSED(defaultSelected);
    dbo = pdb;
    setUp();
    ui->defaultSeller->setChecked(true);
}


SellerForm::SellerForm(QWidget *parent, DatabaseOperator *pdb) :
    QWidget(parent),
    ui(new Ui::SellerForm) {
    dbo = pdb;
    setUp();
    addSeller = new QPushButton("Dodaj sprzedawcę");
    removeSeller = new QPushButton("Usuń sprzedawcę");
    saveChanges = new QPushButton("Zapisz zmiany");
    ids = new QComboBox(this);
    loadIDs();
    ui->leftBox->addWidget(ids, Qt::AlignTop | Qt::AlignHCenter);
    ui->leftBox->addWidget(addSeller);
    ui->leftBox->addWidget(removeSeller);
    ui->leftBox->addWidget(saveChanges);

    loadSellerInfo();

    connect(addSeller, &QPushButton::clicked, this, &SellerForm::saveData);
    connect(removeSeller, &QPushButton::clicked, this, &SellerForm::deleteSeller);
    connect(ids, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSeller(int)));
    connect(saveChanges, &QPushButton::clicked, this, &SellerForm::saveData);
}


void SellerForm::setUp() {
    ui->setupUi(this);
    connect(ui->nip, &QLineEdit::textChanged, this, &SellerForm::validateNIP);
}


void SellerForm::validateNIP() {
    QPalette *p = new QPalette();

    if (Validators::validateNIP(ui->nip->text())) {
        if(Settings::getInstance().getFormatNip()) {
            ui->nip->setText(Validators::formatNIP(ui->nip->text()));
        } else {
            ui->nip->setText(ui->nip->text());
        }

        p->setColor(QPalette::Text, QColor(Qt::black));
    } else {
        p->setColor(QPalette::Text, QColor(Qt::red));
    }

    ui->nip->setPalette(*p);
}


void SellerForm::addNewSeller() {
    Seller *seller = new Seller();
    seller->id = "0";
    seller->company = ui->companyName->text();
    seller->name = ui->nameSurname->text();
    seller->postcode = ui->postcode->text();
    seller->city = ui->city->text();
    seller->street = ui->street->text();
    seller->NIP = ui->nip->text().remove(QRegExp("\\D*"));
    seller->www = ui->www->text();
    seller->email = ui->email->text();
    seller->phone = ui->phone->text();
    seller->bankAccount = ui->bankacc->text();
    seller->rightText = ui->rightsidetext->text();
    seller->bottomText = ui->bottomtext->text();
    seller->isDefault = ui->defaultSeller->isChecked();
    dbo->saveSeller(seller);
}


void SellerForm::loadSellerInfo() {
    Seller *seller = dbo->getDefaultSeller();
    setSeller(seller);
}


void SellerForm::setSeller(Seller *seller) {
    ui->companyName->setText(seller->company);
    ui->nameSurname->setText(seller->name);
    ui->postcode->setText(seller->postcode);
    ui->city->setText(seller->city);
    ui->street->setText(seller->street);
    ui->nip->setText(seller->NIP);
    ui->www->setText(seller->www);
    ui->email->setText(seller->email);
    ui->phone->setText(seller->phone);
    ui->bankacc->setText(seller->bankAccount);
    ui->rightsidetext->setText(seller->rightText);
    ui->bottomtext->setText(seller->bottomText);
    ui->defaultSeller->setChecked(seller->isDefault);
}


void SellerForm::saveData() {
    Seller *seller = new Seller();
    seller->id = ids->currentText();
    seller->company = ui->companyName->text();
    seller->name = ui->nameSurname->text();
    seller->postcode = ui->postcode->text();
    seller->city = ui->city->text();
    seller->street = ui->street->text();
    seller->NIP = ui->nip->text().remove(QRegExp("\\D*"));
    seller->www = ui->www->text();
    seller->email = ui->email->text();
    seller->phone = ui->phone->text();
    seller->bankAccount = ui->bankacc->text();
    seller->rightText = ui->rightsidetext->text();
    seller->bottomText = ui->bottomtext->text();
    seller->isDefault = ui->defaultSeller->isChecked();
    dbo->saveSeller(seller);
}


void SellerForm::changeSeller(int unused) {
    Q_UNUSED(unused);
    Seller *seller = new Seller();
    seller->NIP = ids->currentText();
    setSeller(dbo->getSeller(seller));
}


void SellerForm::deleteSeller() {
    Seller *seller = new Seller();
    seller->NIP = ui->nip->text().remove(QRegExp("\\D*"));
    dbo->deleteSeller(seller);
    loadSellerInfo();
    ids->removeItem(ids->currentIndex());
    ids->setCurrentIndex(0);
}


void SellerForm::loadIDs() {
    ids->addItems(dbo->sellersIDs());
}


SellerForm::~SellerForm() {
    delete ui;
}
