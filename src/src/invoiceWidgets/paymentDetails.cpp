#include "paymentDetails.h"
#include "ui_paymentDetails.h"
#include <math.h>
#include <limits>

PaymentDetails::PaymentDetails(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PaymentDetails) {
    ui->setupUi(this);
    double doubleMax = std::numeric_limits<double>::max();
    ui->alreadyPaid->setRange(-doubleMax, doubleMax);
    ui->toPay->setRange(-doubleMax, doubleMax);
    ui->alreadyPaid->setMaximumWidth(120);
    ui->toPay->setMaximumWidth(120);
    QDate date = QDate::currentDate();
    date = date.addDays((qint64) Settings::getInstance().getDaysForPayment());
    ui->paymentDeadline->setDate(date);
    connect(ui->swapValues, &QPushButton::clicked, this, &PaymentDetails::swapValues);
    connect(ui->alreadyPaidSumUp, &QPushButton::clicked, this, &PaymentDetails::onAlreadyPaidSumUp);
    connect(ui->toPaySumUp, &QPushButton::clicked, this, &PaymentDetails::onToPaySumUp);
}

void PaymentDetails::onToPaySumUp() {
    double valuePaid = ui->alreadyPaid->value(),
           valueToPay= ui->toPay->value();
    ui->toPay->setValue(valuePaid + valueToPay);
    ui->alreadyPaid->setValue((double) 0);
}

void PaymentDetails::onAlreadyPaidSumUp() {
    double valuePaid = ui->alreadyPaid->value(),
           valueToPay= ui->toPay->value();
    ui->alreadyPaid->setValue(valuePaid + valueToPay);
    ui->toPay->setValue((double) 0);
}

void PaymentDetails::setDeadlineDate(QDate date) {
    ui->paymentDeadline->setDate(date);
}

QDate PaymentDetails::deadlineDate() {
    return ui->paymentDeadline->date();
}

QGroupBox *PaymentDetails::groupBox() {
    return ui->groupBox;
}

void PaymentDetails::clearPaymentMethods() {
    ui->paymentMethods->clear();
}

void PaymentDetails::setPaymentMethods(QStringList methods) {
    ui->paymentMethods->addItems(methods);
}

QString PaymentDetails::paymentMethod() {
    return ui->paymentMethods->currentText();
}

void PaymentDetails::swapValues() {
    double valuePaid = ui->alreadyPaid->value(),
           valueToPay= ui->toPay->value();
    ui->toPay->setValue(valuePaid);
    ui->alreadyPaid->setValue(valueToPay);
}

void PaymentDetails::setAlreadyPaid(double value) {
    ui->alreadyPaid->setValue(value);
}

void PaymentDetails::setToPay(double value) {
    ui->toPay->setValue(value);
}

double PaymentDetails::alreadyPaid() {
    return ui->alreadyPaid->value();
}

double PaymentDetails::toPay() {
    return ui->toPay->value();
}

PaymentDetails::~PaymentDetails() {
    delete ui;
}
