#ifndef ITEMSTABLE_H
#define ITEMSTABLE_H

#include "data/Item.h"
#include "../InvoiceTab.h"

#include <QMessageBox>
#include <QComboBox>
#include <QList>
#include <QStringList>
#include <QTableWidgetItem>

class ItemsTable : public QTableWidget {
        Q_OBJECT

    public:
        ItemsTable(InvoiceTab *parent);
        ~ItemsTable();

        Item *getItemFromRow(int row);
        QList<Item *> *getAllItems();
        void insertItems(QList<Item *> *items);
        void removeRow(int row);
        double sumUpGross();
        double sumUpNet();
        void removeAllRows();
        void insertItem(int row, Item *item);

        bool byUser;
        int lastEditedRow;

    public slots:
        void computePrices(int row, int column);
        void changeSchema();
        void addRow();
        void deleteRow();

    private:
        InvoiceTab *parent;
        QString defaultVat;

        QStringList measureUnits,
                    vats;
        QList<QComboBox *> measureUnitsCB,
              vatsCB;

        void setVatsAndMeasures();
        void setCellDoubleValue(int row, int column, double value);
        double getValueForCell(int row, int column);

    private slots:
        void onVATChanged(int i);

};

#endif // ITEMSTABLE_H
