#ifndef INVOICEDETAILS_H
#define INVOICEDETAILS_H

#include <QWidget>
#include <QGroupBox>
#include <QDate>

namespace Ui {
    class InvoiceDetails;
}

class InvoiceDetails : public QWidget {
        Q_OBJECT

    public:
        explicit InvoiceDetails(QWidget *parent = 0);
        ~InvoiceDetails();
        QGroupBox *groupBox();
        QDate productionDate();
        QDate sellingDate();
        void setProductionDate(QDate date);
        void setSellingDate(QDate date);
        QString city();
        QString invoiceID();
        void setCity(QString city);
        void setInvoiceID(QString invoiceID);

    private:
        Ui::InvoiceDetails *ui;
};

#endif // INVOICEDETAILS_H
