#ifndef PAYMENTDETAILS_H
#define PAYMENTDETAILS_H

#include <QWidget>
#include <QStringList>
#include <QGroupBox>
#include "data/Settings.h"

namespace Ui {
    class PaymentDetails;
}

class PaymentDetails : public QWidget {
        Q_OBJECT

    public:
        explicit PaymentDetails(QWidget *parent = 0);
        ~PaymentDetails();
        double alreadyPaid();
        double toPay();
        void setAlreadyPaid(double value);
        void setToPay(double value);
        QString paymentMethod();
        void setPaymentMethods(QStringList);
        void clearPaymentMethods();
        QGroupBox *groupBox();
        void setDeadlineDate(QDate date);
        QDate deadlineDate();

    private:
        Ui::PaymentDetails *ui;

    private slots:
        void swapValues();
        void onAlreadyPaidSumUp();
        void onToPaySumUp();

};

#endif // PAYMENTDETAILS_H
