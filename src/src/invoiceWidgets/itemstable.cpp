#include "itemstable.h"
#include <QDebug>

#define PRODUCTNAME 0
#define MEASURE     1
#define QUANTITY    2
#define SINGLEPRICE 3
#define NET         4
#define VAT         5
#define GROSS       6

ItemsTable::ItemsTable(InvoiceTab *parent) {
    this->parent = parent;
    QStringList columns;
    columns << "Nazwa produktu" << "Miara"
            << QString::fromUtf8("Ilość") << "CJ"
            << "Netto" << "VAT" << "Brutto";
    this->setColumnCount(columns.size());
    this->setEnabled(true);
    this->setHorizontalHeaderLabels(columns);
    this->setColumnWidth(PRODUCTNAME, 160);
    this->setColumnWidth(MEASURE, 50);
    this->setColumnWidth(QUANTITY, 50);
    this->setColumnWidth(SINGLEPRICE, 50);
    this->setColumnWidth(NET, 160);
    this->setColumnWidth(VAT, 50);
    this->setColumnWidth(GROSS, 160);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setAlternatingRowColors(true);
    addRow();
    sumUpGross();
    ///TODO this must be fixed later
    this->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    connect(this, &ItemsTable::cellDoubleClicked, this, &ItemsTable::changeSchema);
    connect(this, &ItemsTable::cellChanged, this, &ItemsTable::computePrices);
    connect(this, &ItemsTable::cellPressed, this, &ItemsTable::computePrices);
}

void ItemsTable::computePrices(int row, int column) {
    if (byUser == true) {
        byUser = false;
        bool ok = true;
        double quantity = 1,
               singlePrice = 1.0,
               netto = 1.0,
               brutto = 1.0,
               vat = Settings::getInstance().getDefaultVAT().toDouble(&ok);

        if (column == QUANTITY || column == SINGLEPRICE) {
            if (this->item(row, QUANTITY)) {
                quantity = getValueForCell(row, QUANTITY);
            } else {
                setCellDoubleValue(row, QUANTITY, quantity);
            }

            if (this->item(row, SINGLEPRICE)) {
                singlePrice = getValueForCell(row, SINGLEPRICE);
            }

            netto = quantity * singlePrice;
            setCellDoubleValue(row, NET, netto);
            vat = (vatsCB[row]->currentText().toDouble(&ok) + 100) / 100;
            brutto = netto * vat;
            setCellDoubleValue(row, GROSS, brutto);
        } else if (column == NET) {
            vat = (vatsCB[row]->currentText().toDouble(&ok) + 100) / 100;
            netto = getValueForCell(row, NET);
            brutto = netto * vat;
            setCellDoubleValue(row, GROSS, brutto);
            quantity = getValueForCell(row, QUANTITY);
            singlePrice = netto / quantity;
            setCellDoubleValue(row, SINGLEPRICE, singlePrice);
        } else if (column == GROSS) {
            vat = vatsCB[row]->currentText().toDouble(&ok) + 100;
            brutto = getValueForCell(row, column);
            netto = brutto * (100.0 / vat);
            setCellDoubleValue(row, NET, netto);

            // compute quantity from net
            if (this->item(row, QUANTITY) == NULL && this->item(row, SINGLEPRICE) != NULL) {
                singlePrice = getValueForCell(row, SINGLEPRICE);
                quantity = netto / singlePrice;
                setCellDoubleValue(row, QUANTITY, quantity);
            }

            // compute single price from netto and quantity
            if (this->item(row, SINGLEPRICE) == NULL && this->item(row, QUANTITY) != NULL) {
                quantity = getValueForCell(row, QUANTITY);
                singlePrice = netto / quantity;
                setCellDoubleValue(row, SINGLEPRICE, singlePrice);
            }

            //this doesn't do anything oO
            // update price if both cells aren't NULL
//            if (this->item(row, QUANTITY) != NULL && this->item(row, SINGLEPRICE) != NULL) {
//                quantity = getValueForCell(row, QUANTITY);
//                singlePrice = netto / quantity;
//            }
        } // if it was column 2nd or 3rd

        sumUpGross();
    } //if byUser == true
} //computePrices

void ItemsTable::setCellDoubleValue(int row, int column, double value) {
    this->setItem(row, column, new QTableWidgetItem(QString::number(value, 'f', 2)));
}

double ItemsTable::getValueForCell(int row, int column) {
    bool ok = true;
    return this->item(row, column)->text().replace(',', '.').toDouble(&ok);
}

Item *ItemsTable::getItemFromRow(int row) {
    Item *item = nullptr;

    if(this->item(row, PRODUCTNAME) != nullptr) {
        item = new Item();
        bool ok = true;
        item->setName(this->item(row, PRODUCTNAME)->text());
        item->setMeasure(measureUnitsCB.at(row)->currentText());
        item->setQuantity(this->item(row, QUANTITY)->text().toDouble(&ok));
        item->setSinglePrice(this->item(row, SINGLEPRICE)->text().toDouble(&ok));
        item->setNetto(this->item(row, NET)->text().toDouble(&ok));
        item->setVat(vatsCB.at(row)->currentText().toInt(&ok, 10));
        item->setGross(this->item(row, GROSS)->text().toDouble(&ok));
    }

    return item;
}


QList<Item *> *ItemsTable::getAllItems() {
    QList<Item *> *items = new QList<Item *>();

    for(int i = 0; i < this->rowCount(); i++) {
        Item *tmpItem = this->getItemFromRow(i);

        if(tmpItem != nullptr) {
            items->append(tmpItem);
        }
    }

    return items;
}


void ItemsTable::changeSchema() {
    byUser = true;
}


void ItemsTable::addRow() {
    int row = this->rowCount();
    this->insertRow(row);
    setVatsAndMeasures();
    //
    measureUnitsCB.append(new QComboBox());
    measureUnitsCB.last()->addItems(measureUnits);
    //
    vatsCB.append(new QComboBox());
    vatsCB.last()->addItems(vats);
    vatsCB.last()->setCurrentText(QString(Settings::getInstance().getDefaultVAT()));
    this->setItem(row, PRODUCTNAME, new QTableWidgetItem(""));
    this->setCellWidget(row, MEASURE, measureUnitsCB.last());
    this->setItem(row, QUANTITY, new QTableWidgetItem("1.00"));
    this->setItem(row, SINGLEPRICE, new QTableWidgetItem("1.00"));
    this->setItem(row, NET, new QTableWidgetItem("1.00"));
    this->setCellWidget(row, VAT, vatsCB.last());
    this->setItem(row, GROSS, new QTableWidgetItem(
                      QString::number(
                          (vatsCB.last()->currentText().toDouble(0) / 100.0)
                          * this->item(row, NET)->text().toDouble()
                          + this->item(row, NET)->text().toDouble()
                          , 'f', 2)));
    // not sure if activated is ok here
    // SLOT should be called 'droped' or 'selected', but activated is better than
    // textChanged, sometimes text is not changed, after user selected the same
    // value from the list, just to recompute gross column
    connect(vatsCB.last(), static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, &ItemsTable::onVATChanged);
    sumUpGross(); //this will change toPay value in invoiceTab
} //addRow


void ItemsTable::insertItems(QList<Item *> *items) {
    for(int i = 0; i < this->rowCount(); i++) {
        this->QTableWidget::removeRow(0);
        measureUnitsCB.removeAt(0);
        vatsCB.removeAt(0);
    }

    sumUpGross();

    for(int i = 0; i < items->size(); i++) {
        addRow();
        insertItem(this->rowCount() - 1, items->at(i));
    }
}

void ItemsTable::insertItem(int row, Item *item) {
    if(row == -1) {
        addRow();
        row = this->rowCount() - 1;
    }

    this->item(row, PRODUCTNAME)->setText(item->getName());
    this->item(row, QUANTITY)->setText(QString::number(item->getQuantity(), 'f', 2));
    this->item(row, SINGLEPRICE)->setText(QString::number(item->getSinglePrice(), 'f', 2));
    this->item(row, NET)->setText(QString::number(item->getNetto(), 'f', 2));
    this->item(row, GROSS)->setText(QString::number(item->getGross(), 'f', 2));
}


double ItemsTable::sumUpNet() {
    double sum = 0.0;
    bool ok = true;

    for (int i = 0; i < this->rowCount(); i++) {
        if (this->item(i, NET) != nullptr) {
            sum += this->item(i, NET)->text().toDouble(&ok);
        }
    }

    return sum;
}

double ItemsTable::sumUpGross() {
    double sum = 0.0;
    bool ok = true;

    for (int i = 0; i < this->rowCount(); i++) {
        if (this->item(i, GROSS) != nullptr) {
            sum += this->item(i, GROSS)->text().toDouble(&ok);
        }
    }

    parent->setToPay(sum);
    return sum;
}


void ItemsTable::removeAllRows() {
    for(int i = 0; i < this->rowCount(); i++) {
        this->removeRow(0);
        measureUnitsCB.removeAt(0);
        vatsCB.removeAt(0);
    }
}


void ItemsTable::deleteRow() {
    int row = this->currentRow();
    this->removeRow(row);
    measureUnitsCB.removeAt(row);
    vatsCB.removeAt(row);
}


void ItemsTable::removeRow(int row) {
    if(row >= 0) {
        this->QTableWidget::removeRow(row); //otherwise method would call itself
        sumUpGross();
    }

    if(this->rowCount() == 0) {
        addRow();
    }

    sumUpGross(); //this will change toPay value in invoiceTab
}


void ItemsTable::setVatsAndMeasures() {
    vats.clear();
    measureUnits.clear();
    vats << Settings::getInstance().getDefaultVAT()
         << Settings::getInstance().getTaxes().replace(" ", "").split(",");
    measureUnits << Settings::getInstance().getMeasures().replace(" ", "").split(",");
    vats.removeDuplicates();
    measureUnits.removeDuplicates();
}


void ItemsTable::onVATChanged(int index) {
    Q_UNUSED(index);
    int row = 0;

    for(int i = 0; i < vatsCB.size(); i++) {
        if(QObject::sender() == vatsCB.at(i)) {
            row = i;
            break;
        }
    }

    double vat = vatsCB.at(row)->currentText().toDouble(0) / 100.0;
    double net = this->item(row, NET)->text().toDouble(0);
    double gross = vat * net + net;
    this->item(row, GROSS)->setText(QString::number(gross, 'f', 2));
    sumUpGross();
}


ItemsTable::~ItemsTable() {
}
