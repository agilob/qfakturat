#include "extrasettings.h"
#include "ui_extrasettings.h"

ExtraSettings::ExtraSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExtraSettings) {
    ui->setupUi(this);
    ui->currenciesCb->addItems(Settings::getInstance().getCurrencies().split(","));
    ui->formatNip->setChecked(Settings::getInstance().getFormatNip());
    connect(ui->formatNip, &QCheckBox::toggled, this, &ExtraSettings::onFormatNipChecked);
    this->setGeometry(QStyle::alignedRect(
                          Qt::LeftToRight,
                          Qt::AlignCenter,
                          this->size(),
                          QApplication::desktop()->availableGeometry()));
    this->show();
}


void ExtraSettings::onFormatNipChecked(bool state) {
    Settings::getInstance().setFormatNip(state);
    Settings::getInstance().save();
}


QString ExtraSettings::getCurrency() {
    return ui->currenciesCb->currentText();
}


ExtraSettings::~ExtraSettings() {
    delete ui;
}
