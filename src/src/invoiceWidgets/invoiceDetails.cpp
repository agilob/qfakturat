#include "invoiceDetails.h"
#include "ui_invoiceDetails.h"

InvoiceDetails::InvoiceDetails(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InvoiceDetails) {
    ui->setupUi(this);
    ui->invoiceID->setText("/" + QDate::currentDate().toString("MM/yyyy"));
    ui->sellingDate->setDate(QDate::currentDate());
    ui->productionDate->setDate(QDate::currentDate());
}

QDate InvoiceDetails::productionDate() {
    return ui->productionDate->date();
}

QDate InvoiceDetails::sellingDate() {
    return ui->sellingDate->date();
}

void InvoiceDetails::setProductionDate(QDate date) {
    ui->productionDate->setDate(date);
}

void InvoiceDetails::setSellingDate(QDate date) {
    ui->sellingDate->setDate(date);
}

QString InvoiceDetails::city() {
    return ui->city->text();
}

QString InvoiceDetails::invoiceID() {
    return ui->invoiceID->text();
}

void InvoiceDetails::setCity(QString city) {
    ui->city->setText(city);
}

void InvoiceDetails::setInvoiceID(QString invoiceID) {
    ui->invoiceID->setText(invoiceID);
}

QGroupBox *InvoiceDetails::groupBox() {
    return ui->groupBox;
}

InvoiceDetails::~InvoiceDetails() {
    delete ui;
}
