#ifndef EXTRASETTINGS_H
#define EXTRASETTINGS_H

#include <QWidget>
#include <QDialog>
#include <QDesktopWidget>

#include "data/Settings.h"

namespace Ui {
    class ExtraSettings;
}

class ExtraSettings : public QDialog {
        Q_OBJECT

    public:
        explicit ExtraSettings(QWidget *parent = 0);
        ~ExtraSettings();
        QString getCurrency();

    private:
        Ui::ExtraSettings *ui;

    private slots:
        void onFormatNipChecked(bool);
};

#endif // EXTRASETTINGS_H
