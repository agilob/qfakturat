#ifndef PRODUCTSHISTORY_H
#define PRODUCTSHISTORY_H

#include "TabPanel.h"
#include <QWidget>

namespace Ui {
    class ProductsHistory;
}

class TabPanel;

class ProductsHistory : public QWidget {
        Q_OBJECT

    public:
        ProductsHistory(TabPanel *parent);
        ~ProductsHistory();

    protected:
        void changeEvent(QEvent *e);

    private:
        Ui::ProductsHistory *ui;
        QList<QPushButton *> *buttons;
        TabPanel *parent;
        QList<Item *> *items;
        double minPrice;
        double maxPrice;
        QString vat;
        QString name;
        double getMaxPrice();
        void applyFilters();
        QStringList getListOfVats();

    private slots:
        void locateButtonAndSendItem();
        void onNameChanged(QString filter);
        void onMinPriceChanged(double price);
        void onMaxPriceChanged(double price);
        void onVatChanged(QString);

};

#endif // PRODUCTSHISTORY_H
