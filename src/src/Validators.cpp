#include "Validators.h"

#include <QDebug>

Validators::Validators() {
}

Validators::~Validators() {
}

bool Validators::validateNIP(QString nip) {
    bool valid = false,
         ok = true;

    // remove some special characters
    nip = nip.remove(QRegExp("[-.~!@#$%^&*()_+}{|\"::?><]"));

    // remove Locale like PL,DE,GB from NIP if seller/buyer uses
    // country specific NIP code
    if(nip.contains(QRegExp("^[a-zA-z]{2}"))) {
        nip = nip.remove(0, 2);
    }

    if(nip.length() == 0) {
        return false;
    }

    if (nip.contains(QRegExp("^[0-9]{8}"))) {
        int checkSum = QString(nip[nip.length() - 1]).toInt(&ok, 10);
        nip.chop(1);
        int wages[] = {6, 5, 7, 2, 3, 4, 5, 6, 7};
        int counter = 0;

        for (int i = 0; i < 9; i++) {
            counter = wages[i] * QString(nip[i]).toInt(&ok, 10) + counter;
        }

        if (counter % 11 == checkSum) {
            valid = true;
        }
    } else {
        valid = false;
    }

    return valid;
}

QString Validators::formatNIP(QString nip) {
    return nip.left(3) + "-" +
           nip.mid(3, 3) + "-" +
           nip.mid(6, 2) + "-" +
           nip.right(2);
}
