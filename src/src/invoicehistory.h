#ifndef INVOICEHISTORY_H
#define INVOICEHISTORY_H

#include "TabPanel.h"
#include "invoiceview.h"
#include "DatabaseOperator.h"

#include <QWidget>
#include <QMenu>
#include <QAction>


namespace Ui {
    class InvoiceHistory;
}

class TabPanel;

class InvoiceHistory : public QWidget {
        Q_OBJECT

    public:
        InvoiceHistory(TabPanel *tmpParent);
        ~InvoiceHistory();

    private:
        Ui::InvoiceHistory *ui;
        QList< Invoice *> *invoices;
        DatabaseOperator *dbO;
        TabPanel *parent;

    public slots:
        void onCellDoubleClicked(int row, int column);
        void onLoadButtonClicked();
        void deleteInvoice();
        void previewInvoice();
        void displayMenu(const QPoint &pos);

};

#endif // INVOICEHISTORY_H
