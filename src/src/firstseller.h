#ifndef FIRSTSELLER_H
#define FIRSTSELLER_H

#include <QWidget>
#include <QProcess>

#include "sellerform.h"
#include "DatabaseOperator.h"

namespace Ui {
    class FirstSeller;
}

class FirstSeller : public QWidget {
        Q_OBJECT

    public:
        explicit FirstSeller(QWidget *parent = 0, DatabaseOperator *pdb = 0);
        ~FirstSeller();

    private:
        Ui::FirstSeller *ui;
        SellerForm *sf;

    private slots:
        void addFirstSeller();

};

#endif // FIRSTSELLER_H
