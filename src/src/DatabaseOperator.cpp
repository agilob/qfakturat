#include "DatabaseOperator.h"

const QString DatabaseOperator::DATABASE_FILENAME = "/database.sqlite3";

DatabaseOperator::DatabaseOperator() {
    db = QSqlDatabase::addDatabase("QSQLITE", "main");
    db.setDatabaseName(getPath());
    seller = new Seller();
    client = new Client();
    listOfClients = new QList<Client *>();
    createDatabase();
    alterTables();
} //databaseOperator


QString DatabaseOperator::getPath() {
    return QDir::toNativeSeparators(
                Settings::getSettingsDirPath() + DATABASE_FILENAME);
} //getPath


void DatabaseOperator::createDatabase() {
    if(db.open()) {
        qDebug() << "loading:" << db;
        query = QSqlQuery(db);
        createTableClients();
        createTableSeller();
        createInvoiceTable();
        createItemTable();
    } else {
        qWarning() << "Warning: unable to open database:" << db;
    }
} //createDatabase


void DatabaseOperator::alterTables() {
    alterTableInvoice();
}


void DatabaseOperator::createTableClients() {
    query = QSqlQuery(db);
    query.exec(
        "CREATE TABLE IF NOT EXISTS clients ( "
        "name varchar(50), "
        "nameAndSurname varchar(70), "
        "city varchar(30), "
        "street varchar(30), "
        "NIP varchar PRIMARY KEY UNIQUE);");
    debugQuery();
} //createTableClients


void DatabaseOperator::createTableSeller() {
    query = QSqlQuery(db);
    query.exec(
               "CREATE TABLE IF NOT EXISTS seller ( "
               "ID INTEGER ROWID, "
               "name varchar(50), "
               "nameAndSurname varchar(70), "
               "postcode varchar(10), "
               "city varchar(30), "
               "street varchar(30), "
               "NIP varchar PRIMARY KEY, "
               "WWW varchar(50), "
               "email varchar(50), "
               "phone varchar(15), "
               "BankAcc varchar(50), "
               "rightHandText varchar(40), "
               "bottomText varchar(25), "
               "isDefault boolean);");
    debugQuery();
} //createTableSeller


void DatabaseOperator::alterTableInvoice() {
    if(!db.record("invoice").contains("currency")) {
        query.exec("ALTER TABLE invoice ADD COLUMN currency varchar(10);");
        debugQuery();
        query.exec("UPDATE invoice SET currency='PLN' WHERE currency is null;");
        debugQuery();
    }

    if(!db.record("invoice").contains("id")) {
        query.exec("ALTER TABLE invoice ADD COLUMN id INTEGER AUTOINC;");
        debugQuery();
        query.exec("UPDATE invoice SET id = id + 1;");
        debugQuery();
    }
}


void DatabaseOperator::createInvoiceTable() {
    query = QSqlQuery(db);
    query.exec(
        "CREATE TABLE IF NOT EXISTS invoice ( "
        "seller varchar, "
        "buyer varchar, "
        "inumber varchar UNIQUE, "
        "city varchar(255), "
        "sellingDate varchar(255), "
        "creatingDate varchar(255), "
        "deadlineDate varchar(255), "
        "paid double, "
        "toPay double, "
        "netto double, "
        "gross double, "
        "payingMethod varchar(255), "
        "info varchar(255), "
        "invoiceType varchar(255), "
        "currency varchar(10), "
        "FOREIGN KEY(seller) REFERENCES seller(NIP), "
        "FOREIGN KEY(buyer) REFERENCES buyer(NIP));");
    debugQuery();
}


void DatabaseOperator::createItemTable() {
    query = QSqlQuery(db);
    query.exec("CREATE TABLE IF NOT EXISTS item ( "
               "invoice varchar NOT NULL, "
               "name varchar, "
               "measure varchar, "
               "quantity double, "
               "singlePrice double, "
               "netto double, "
               "brutto double, "
               "VAT int, "
               "tax double, "
               "FOREIGN KEY(invoice) REFERENCES invoice(ID));");
    debugQuery();
}


bool DatabaseOperator::defaultSellerExists() {
    query = QSqlQuery(db);
    query.exec("SELECT * FROM seller;");
    return query.next();
}

Seller *DatabaseOperator::getDefaultSeller() {
    query = QSqlQuery(db);
    query.exec("SELECT "
               "ID, name, nameAndSurname, postcode, city, street, "
               "NIP, WWW, email, phone, "
               "BankAcc, rightHandText, bottomText, isDefault "
               "FROM seller WHERE isDefault = 1 LIMIT 1;");

    if(query.next()) {
        seller = new Seller();
        seller->company = query.value(1).toString();
        seller->name = query.value(2).toString();
        seller->postcode = query.value(3).toString();
        seller->city = query.value(4).toString();
        seller->street = query.value(5).toString();
        seller->NIP = query.value(6).toString();
        seller->www = query.value(7).toString();
        seller->email = query.value(8).toString();
        seller->phone = query.value(9).toString();
        seller->bankAccount = query.value(10).toString();
        seller->rightText = query.value(11).toString();
        seller->bottomText = query.value(12).toString();
        seller->isDefault = query.value(13).toBool();
    }

    return seller;
} //getDefaultSeller


QList<Client *> *DatabaseOperator::getClients() {
    query = QSqlQuery(db);
    query.exec("SELECT "
               "name, nameAndSurname, city, street, NIP "
               "FROM clients;");

    while (query.next()) {
        client = new Client();
        client->company = query.value(0).toString();
        client->name = query.value(1).toString();
        client->city = query.value(2).toString();
        client->street = query.value(3).toString();
        client->NIP = query.value(4).toString();
        listOfClients->append(client);
    }

    return listOfClients;
} //getClients


QList<Item *> *DatabaseOperator::getItems(QString invoice) {
    QSqlQuery q = QSqlQuery(db);
    q.prepare("SELECT "
              "invoice, name, measure, quantity, "
              "singlePrice, netto, brutto, VAT, tax "
              "FROM item WHERE invoice = :invoice;");
    q.bindValue(":invoice", invoice);
    q.exec();
    QList<Item *> *itemsOfItems = new QList<Item *>;

    while (q.next()) {
        Item *item = new Item();
        item->setName(q.value(1).toString());
        item->setMeasure(q.value(2).toString());
        item->setQuantity(q.value(3).toDouble());
        item->setSinglePrice(q.value(4).toDouble());
        item->setNetto(q.value(5).toDouble());
        item->setGross(q.value(6).toDouble());
        item->setVat(q.value(7).toInt());
        itemsOfItems->prepend(item);
    }

    return itemsOfItems;
} //getItems


Client *DatabaseOperator::getClient(QString nip) {
    Client *client = new Client();
    QSqlQuery q = QSqlQuery(db);
    q.prepare("SELECT clients WHERE NIP = :nip;");
    q.bindValue(":nip", nip);
    query.exec();

    if(q.next()) {
        client->company = q.value(0).toString();
        client->name = q.value(1).toString();
        client->city = q.value(2).toString();
        client->street = q.value(3).toString();
        client->NIP = q.value(4).toString();
    }

    return client;
} //loadClient


Seller *DatabaseOperator::getSeller(Seller *seller) {
    QSqlQuery q = QSqlQuery(db);
    q.prepare("SELECT "
              "ID, name, nameAndSurname, postcode, city, "
              "street, NIP, WWW, email, "
              "phone, BankAcc, rightHandText, "
              "bottomText, isDefault "
              "FROM seller WHERE NIP = :nip;");
    q.bindValue(":nip", seller->NIP);
    q.exec();

    if(q.next()) {
        seller = new Seller();
        seller->company = q.value(1).toString();
        seller->name = q.value(2).toString();
        seller->postcode = q.value(3).toString();
        seller->city = q.value(4).toString();
        seller->street = q.value(5).toString();
        seller->NIP = q.value(6).toString();
        seller->www = q.value(7).toString();
        seller->email = q.value(8).toString();
        seller->phone = q.value(9).toString();
        seller->bankAccount = q.value(10).toString();
        seller->rightText = q.value(11).toString();
        seller->bottomText = q.value(12).toString();
        seller->isDefault = q.value(13).toBool();
    }

    return seller;
} //loadSeller


Client *DatabaseOperator::getClient(Client *client) {
    QSqlQuery q = QSqlQuery(db);
    q.prepare("SELECT "
              "name, nameAndSurname, city, street, NIP "
              "FROM clients WHERE NIP = :nip;");
    q.bindValue(":nip", client->NIP);
    q.exec();

    if(q.next()) {
        client = new Client();
        client->company = q.value(0).toString();
        client->name = q.value(1).toString();
        client->city = q.value(2).toString();
        client->street = q.value(3).toString();
        client->NIP = q.value(4).toString();
    }

    return client;
} //loadClient


QList<Item *> *DatabaseOperator::loadItems() {
    QSqlQuery query = QSqlQuery(db);
    query.exec("SELECT DISTINCT "
               "name, measure, singlePrice, vat, "
               "netto, quantity, brutto "
               "FROM item GROUP BY name ORDER BY invoice ASC;");
    QList<Item *> *listOfItems = new QList<Item * >();

    while(query.next()) {
        Item *item = new Item();
        item->setName(query.value(0).toString());
        item->setMeasure(query.value(1).toString());
        item->setSinglePrice(query.value(2).toDouble());
        item->setVat(query.value(3).toInt());
        item->setNetto(query.value(4).toDouble());
        item->setQuantity(query.value(5).toDouble());
        item->setGross(query.value(6).toDouble());
        listOfItems->append(item);
    }

    return listOfItems;
}


QList<Invoice *> *DatabaseOperator::last100() {
    QList<Invoice *> *listOfInvoices = new QList<Invoice *>();
    query = QSqlQuery(db);
    Invoice *inv;
    Client *client;
    Seller *seller;
    query.prepare("SELECT "
                  "seller, buyer, inumber, invoiceType, city, sellingDate, "
                  "creatingDate, deadlineDate, "
                  "paid, toPay, netto, gross, payingMethod, info, currency "
                  "FROM invoice LIMIT 100;");

    if(query.exec())
        while(query.next()) {
            inv = new Invoice();
            client = new Client();
            seller = new Seller();
            seller->NIP = query.value(0).toString();
            client->NIP = query.value(1).toString();
            inv->invoiceNumber = query.value(2).toString();
            inv->type = query.value(3).toString();
            inv->location = query.value(4).toString();
            inv->sellingDate = inv->sellingDate.fromString(query.value(5).toString(), "dd.MM.yyyy");
            inv->creatingDate = inv->creatingDate.fromString(query.value(6).toString(), "dd.MM.yyyy");
            inv->deadDate = inv->deadDate.fromString(query.value(7).toString(), "dd.MM.yyyy");
            inv->paid = query.value(8).toDouble();
            inv->toPay = query.value(9).toDouble();
            inv->netto = query.value(10).toDouble();
            inv->brutto = query.value(11).toDouble();
            inv->payingMethod = query.value(12).toString();
            inv->info = query.value(13).toString();
            inv->currency = query.value(14).toString();
            inv->seller = getSeller(seller);
            inv->client = getClient(client);
            inv->items = getItems(inv->invoiceNumber);
            listOfInvoices->append(inv);
        }
    debugQuery();
    return listOfInvoices;
} //last100


void DatabaseOperator::saveInvoice(Invoice *invoice) {
    saveClient(invoice->client);
    saveItems(invoice->items, invoice->invoiceNumber);
    query = QSqlQuery(db);
    query.prepare("INSERT OR REPLACE INTO invoice ("
                  "seller, buyer, inumber, "
                  "city, sellingDate, creatingDate, deadlineDate, "
                  "paid, toPay, netto, gross, "
                  "payingMethod, invoiceType, currency, id) "
                  "VALUES ( "
                  ":seller, :buyer, :inumber, "
                  ":city, :sellingDate, :creatingDate, :deadLineDate, "
                  ":paid, :toPay, :netto, :gross, "
                  ":payingMethod, :invoiceType, :currency, (SELECT MAX(id) + 1 FROM invoice));");
    query.bindValue(":seller", invoice->seller->NIP.remove(QRegExp("\\D*")));
    query.bindValue(":buyer", invoice->client->NIP.remove(QRegExp("\\D*")));
    query.bindValue(":inumber", invoice->invoiceNumber);
    query.bindValue(":city", invoice->location);
    query.bindValue(":sellingDate", invoice->sellingDate.toString("dd.MM.yyyy"));
    query.bindValue(":creatingDate", invoice->creatingDate.toString("dd.MM.yyyy"));
    query.bindValue(":deadLineDate", invoice->deadDate.toString("dd.MM.yyyy") );
    query.bindValue(":paid", QString::number(invoice->paid, 'f', 2));
    query.bindValue(":toPay", QString::number(invoice->toPay, 'f', 2));
    query.bindValue(":netto", QString::number(invoice->netto, 'f', 2 ));
    query.bindValue(":gross", QString::number(invoice->brutto, 'f', 2));
    query.bindValue(":payingMethod", invoice->payingMethod);
    query.bindValue(":invoiceType", invoice->type);
    query.bindValue(":currency", invoice->currency);
    query.exec();
    debugQuery();
} //saveInvoice


void DatabaseOperator::saveItems(QList<Item *> *items, QString invoice) {
    for (int i = 0; i < items->size(); i++) {
        query = QSqlQuery(db);
        query.prepare(
            "INSERT INTO item ( "
            "invoice, name, measure, quantity, singlePrice, "
            "netto, brutto, VAT, tax) VALUES ("
            ":invoiceNumber, :itemName, :measure, :quantity, "
            ":singlePrice, :priceNetto, :priceBrutto, :vat, :tax);");
        query.bindValue(":invoiceNumber", invoice);
        query.bindValue(":itemName",items->at(i)->getName());
        query.bindValue(":measure", items->at(i)->getMeasure());
        query.bindValue(":quantity", QString::number(items->at(i)->getQuantity()));
        query.bindValue(":singlePrice", QString::number(items->at(i)->getSinglePrice()));
        query.bindValue(":priceNetto", QString::number(items->at(i)->getNetto()));
        query.bindValue(":priceBrutto", QString::number(items->at(i)->getGross()));
        query.bindValue(":vat", QString::number(items->at(i)->getVat()));
        query.bindValue(":tax", (items->at(i)->getGross() - items->at(i)->getNetto()));
        query.exec();
        debugQuery();
    }
} //saveItems


void DatabaseOperator::saveClient(Client *client) {
    query = QSqlQuery(db);
    query.prepare(
        "INSERT OR REPLACE INTO clients "
        "(name, nameAndSurname, city, street, NIP) "
        "VALUES (:company, :name, :city, :street, :nip);");
    query.bindValue(":company", client->company);
    query.bindValue(":name", client->name);
    query.bindValue(":city", client->city);
    query.bindValue(":street", client->street);
    query.bindValue(":nip", client->NIP.remove(QRegExp("\\D*")));
    query.exec();
    debugQuery();
} //saveClient


void DatabaseOperator::saveSeller(Seller *seller) {
    //if new seller if default, all other sellers cant be set as default
    if (seller->isDefault == true) {
        query.exec("UPDATE seller SET isDefault = 0");
        debugQuery();
    }

    query = QSqlQuery(db);
    query.prepare(
        "INSERT OR REPLACE INTO seller"
        "( ID, name, nameAndSurname, postcode, city, street, NIP, "
        "WWW, email, phone, BankAcc, rightHandText, "
        "bottomText, isDefault )"
        "VALUES ( "
        ":id, :company, :name, :postcode, :city, :street, :nip, :www, :email, "
        ":phone, :bank, :rightHandText, :bottomText, :isDefault);");
    query.bindValue(":id", seller->id);
    query.bindValue(":company", seller->company);
    query.bindValue(":name", seller->name);
    query.bindValue(":postcode", seller->postcode);
    query.bindValue(":city", seller->city);
    query.bindValue(":street", seller->street);
    query.bindValue(":nip", seller->NIP);
    query.bindValue(":www", seller->www);
    query.bindValue(":email", seller->email);
    query.bindValue(":phone", seller->phone);
    query.bindValue(":bank", seller->bankAccount);
    query.bindValue(":rightHandText", seller->rightText);
    query.bindValue(":bottomText", seller->bottomText);
    query.bindValue(":isDefault", (seller->isDefault ? (int) 1 : (int) 0));
    query.exec();
    debugQuery();
} //saveSeller


void DatabaseOperator::deleteSeller(Seller *seller) {
    query = QSqlQuery(db);
    query.prepare("DELETE FROM seller WHERE NIP = :nip;");
    query.bindValue(":nip", seller->NIP);
    query.exec();
    debugQuery();
} //deleteSeller


QStringList DatabaseOperator::sellersIDs() {
    QStringList ids;
    query = db.exec("SELECT nip FROM seller ORDER BY isDefault DESC;");

    while (query.next()) {
        ids.append(query.value(0).toString());
    }

    debugQuery();
    return ids;
} //sellersIDs


void DatabaseOperator::deleteInvoice(Invoice *invoice, bool deleteItems) {
    query = QSqlQuery(db);
    query.prepare("DELETE FROM invoice WHERE inumber = :invoice;");
    query.bindValue(":invoice", invoice->invoiceNumber);
    query.exec();
    debugQuery();

    if(deleteItems) {
        deleteProducts(invoice);
    }
} //deleteInvoice


void DatabaseOperator::deleteProducts(Invoice *invoice) {
    query = QSqlQuery(db);
    query.prepare("DELETE FROM item WHERE invoice = :invoice;");
    query.bindValue(":invoice", invoice->invoiceNumber);
    query.exec();
    debugQuery();
}


QString DatabaseOperator::lastInvoiceId() {
    query = QSqlQuery(db);
    QString inumber = "";
    query.exec("SELECT inumber FROM invoice ORDER BY id DESC LIMIT 1;");

    if(query.next()) {
        inumber = query.value(0).toString();
    }

    debugQuery();
    return inumber;
}


void DatabaseOperator::debugQuery() {
    if (QString(getenv("QFDEBUG")) == "on") {
        qDebug() << "LAST QUERY: " << query.lastQuery();
        qDebug() << "ROWS AFFECTED: " << query.numRowsAffected();
        qCritical() << "LAST ERROR: " << query.lastError().text();
    }

    query.finish();
} //debugQuery


void DatabaseOperator::closeDatabase() {
    db.close();
} //closeDatabase


DatabaseOperator::~DatabaseOperator() {
    closeDatabase();
} //destructor
