#ifndef DATABASESETTINGS_H
#define DATABASESETTINGS_H

#include <QWidget>
#include "data/Settings.h"

namespace Ui {
    class DatabaseSettings;
}

class DatabaseSettings : public QWidget {
        Q_OBJECT

    public:
        DatabaseSettings(QWidget *parent = 0);
        ~DatabaseSettings();

    private slots:
        void saveSettings();
        void onSystemThemeEnabled();
        void onSystemThemeChanged(QString theme);

    private:
        Ui::DatabaseSettings *ui;
        void setSettings();

};

#endif // DATABASESETTINGS_H
