#include "SettingsTab.h"

SettingsTab::SettingsTab(QWidget *parent, DatabaseOperator *pdb) : QWidget(parent) {
    gridLayout = new QGridLayout();
    leftMenuBox = new QGroupBox("Ustawienia");
    leftMenuLayout = new QVBoxLayout();
    /*
     * Add buttons to change settings.
     */
    sellerSettingsButton = new QPushButton("Ustawienia sprzedawcy");
    databaseSettingsButton = new QPushButton("Ustawienia programu");
    sf = new SellerForm(this, pdb);
    databaseSettings = new DatabaseSettings(this);
    databaseSettings->hide();
    connect(databaseSettingsButton, &QPushButton::clicked,
            this, &SettingsTab::changeWidgetToDatabase);
    connect(sellerSettingsButton, &QPushButton::clicked,
            this, &SettingsTab::changeWidgetToSellerSettings);
    leftMenuLayout->addWidget(sellerSettingsButton);
    leftMenuLayout->addWidget(databaseSettingsButton);
    leftMenuBox->setLayout(leftMenuLayout);
    field = sf;
    gridLayout->addWidget(leftMenuBox, 0, 0, Qt::AlignLeft);
    gridLayout->addWidget(field, 0, 1);
    this->setLayout(gridLayout);
}


void SettingsTab::changeWidgetToDatabase() {
    field->hide();
    field = databaseSettings;
    gridLayout->addWidget(field, 0, 1);
    field->show();
}


void SettingsTab::changeWidgetToSellerSettings() {
    field->hide();
    field = sf;
    gridLayout->addWidget(field, 0, 1);
    field->show();
}
