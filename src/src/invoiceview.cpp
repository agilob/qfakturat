#include "invoiceview.h"
#include "ui_invoiceview.h"

#define PRODUCTNAME 0
#define MEASURE     1
#define QUANTITY    2
#define SINGLEPRICE 3
#define NET         4
#define VAT         5
#define GROSS       6

InvoiceView::InvoiceView(Invoice *inv, QWidget *parent) : QDialog(parent), ui(new Ui::InvoiceView) {
    ui->setupUi(this);
    connect(ui->closeButton, SIGNAL(clicked()), this, SLOT(close()));
    QStringList columns;
    columns << "Nazwa produktu" << "Miara" << QString::fromUtf8("Ilość") << "CJ"
            << "Netto" << "VAT" << "Brutto";
    ui->table->setColumnCount(columns.size());
    ui->table->setEnabled(true);
    ui->table->setHorizontalHeaderLabels(columns);
    ui->table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    setInvoice(inv);
    this->show();
    //move to the middle of the main screen
    this->setGeometry(QStyle::alignedRect(
                          Qt::LeftToRight,
                          Qt::AlignCenter,
                          this->size(),
                          QApplication::desktop()->availableGeometry()
                      )
                     );
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding );
}

void InvoiceView::setInvoice(Invoice *inv) {
    ui->creationDate->setText(inv->location + " " + inv->creatingDate.toString("dd/MM/yyyy"));
    ui->soldDate->setText(inv->sellingDate.toString("dd/MM/yyyy"));
    ui->deadlineDate->setText(inv->deadDate.toString("dd/MM/yyyy"));
    // setting seller
    ui->sellerCompanyName->setText("\t" + inv->seller->company);
    ui->sellerName->setText("\t" + inv->seller->name);
    ui->sellerCity ->setText("\t" + inv->seller->city);
    ui->sellerStreet ->setText("\t" + inv->seller->street);
    ui->sellerNIP ->setText("\t" + inv->seller->NIP);
    // setting client
    ui->clientCompanyName ->setText("\t" + inv->client->name);
    ui->clientName ->setText("\t" + inv->client->name);
    ui->clientCity ->setText("\t" + inv->client->city);
    ui->clientStreet ->setText("\t" + inv->client->street);
    ui->clientNIp->setText("\t" + inv->client->NIP);

    for(int row = 0; row < inv->items->size(); row++) {
        ui->table->insertRow(row);
        ui->table->setItem(row, PRODUCTNAME, new QTableWidgetItem(inv->items->at(row)->getName()));
        ui->table->setItem(row, MEASURE, new QTableWidgetItem(inv->items->at(row)->getMeasure()));
        ui->table->setItem(row, QUANTITY, new QTableWidgetItem(QString::number(inv->items->at(row)->getQuantity())));
        ui->table->setItem(row, SINGLEPRICE, new QTableWidgetItem(QString::number(inv->items->at(row)->getSinglePrice())));
        ui->table->setItem(row, NET, new QTableWidgetItem(QString::number(inv->items->at(row)->getNetto())));
        ui->table->setItem(row, VAT, new QTableWidgetItem(QString::number(inv->items->at(row)->getVat())));
        ui->table->setItem(row, GROSS, new QTableWidgetItem(QString::number(inv->items->at(row)->getGross())));
    }

    for(int i = 0; i < ui->table->columnCount(); i++) {
        for(int j = 0; j < ui->table->rowCount(); j++) {
            ui->table->item(j, i)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        }
    }

    ui->sumNetto->setText(QString::number(inv->netto, 'f', 2));
    ui->sumVAT->setText(QString::number((inv->brutto - inv->netto), 'f', 2));
    ui->sumGross->setText(QString::number(inv->brutto, 'f', 2));
    ui->toPay->setText(QString::number(inv->toPay, 'f', 2));
}

InvoiceView::~InvoiceView() {
    delete ui;
}
