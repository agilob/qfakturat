#ifndef CLIENTSSELECTOR_H
#define CLIENTSSELECTOR_H

#include "data/Client.h"

#include <QWidget>
#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QApplication>

class InvoiceTab;
class DatabaseOperator;

/**
 * @brief Windows with a table where user can select client's details to load to invoiceTab.
 */
class ClientsSelector : public QDialog {
        Q_OBJECT

    public:
        ClientsSelector(InvoiceTab *parent, QList<Client *> *clients);
        QTableWidget *table; ///< Table with details about clients

    private:
        QStringList columns; ///< A number of columns in the table
        QPushButton *accept; ///< Push event will cause loading details to invoiceTab
        QGridLayout *layout; ///< Layout in window
        QList<Client *> *clients; ///< a list of clients in the table

};

#endif  /* CLIENTSSELECTOR_H */
