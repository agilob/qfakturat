#include "invoicehistory.h"
#include "ui_invoicehistory.h"

class TabPanel;

InvoiceHistory::InvoiceHistory(TabPanel *tmpParent) : ui(new Ui::InvoiceHistory) {
    parent = tmpParent;
    ui->setupUi(this);
    dbO = tmpParent->dbo;
    invoices = dbO->last100();
    ui->table->setRowCount(invoices->size());
    ui->table->setColumnCount(6);
    QStringList headerNames;
    headerNames.append(QString::fromUtf8("Sprzedający"));
    headerNames.append(QString::fromUtf8("Kupujący"));
    headerNames.append(QString::fromUtf8("Faktura"));
    headerNames.append(QString::fromUtf8("Netto"));
    headerNames.append(QString::fromUtf8("Brutto"));
    headerNames.append(QString::fromUtf8("Produkty"));
    ui->table->setHorizontalHeaderLabels(headerNames);
    ui->table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->table->setContextMenuPolicy(Qt::CustomContextMenu);
    //
    connect(ui->table, &QTableWidget::cellDoubleClicked, this, &InvoiceHistory::onCellDoubleClicked);
    connect(ui->preview, &QPushButton::clicked, this, &InvoiceHistory::previewInvoice);
    connect(ui->load, &QPushButton::clicked, this, &InvoiceHistory::onLoadButtonClicked);
    connect(ui->remove, &QPushButton::clicked, this, &InvoiceHistory::deleteInvoice);
    connect(ui->table, &QTableWidget::customContextMenuRequested, this, &InvoiceHistory::displayMenu);

    for (int i = 0; i < invoices->size(); i++) {
        QStringList listOfItems;
        QString items;
        ui->table->setItem(i, 0, new QTableWidgetItem(
                               invoices->at(i)->seller->company +
                               "\n" + invoices->at(i)->seller->NIP));
        ui->table->setItem(i, 1, new QTableWidgetItem(
                               invoices->at(i)->client->company +
                               "\n" + invoices->at(i)->client->NIP));
        ui->table->setItem(i, 2, new QTableWidgetItem(
                               invoices->at(i)->invoiceNumber));
        ui->table->setItem(i, 3, new QTableWidgetItem(QString::number(
                               invoices->at(i)->netto, 'f', 2)));
        ui->table->setItem(i, 4, new QTableWidgetItem(QString::number(
                               invoices->at(i)->brutto, 'f', 2)));

        for(int j = 0; j < invoices->at(i)->items->size(); j++) {
            listOfItems << invoices->at(i)->items->at(j)->getName();
        }

        for(int j = 0; j < invoices->at(i)->items->size() && j < 3; j++) {
            //limit to 3 items per invoice in a row
            items += listOfItems.at(j) + "\n";
        }

        ui->table->setItem(i, 5, new QTableWidgetItem(items));

        //align text in columns
        for (int k = 0; k <= 5; k++) {
            ui->table->item(i, k)->setTextAlignment(/*Qt::AlignHCenter | */Qt::AlignTop);
            ui->table->item(i, k)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        }
    }

    ui->table->resizeRowsToContents();
    ui->table->setWordWrap(true);
}


void InvoiceHistory::displayMenu(const QPoint &pos) {
    QMenu menu(this);
    QAction *show = menu.addAction("Podgląd");
    QAction *load = menu.addAction("Wczytaj");
    QAction *del = menu.addAction("Usuń");
    QAction *action = menu.exec(ui->table->viewport()->mapToGlobal(pos));

    if (action == show) {
        previewInvoice();
    } else if (action == load) {
        onLoadButtonClicked();
    } else if (action == del) {
        deleteInvoice();
    }
}

void InvoiceHistory::previewInvoice() {
    if(ui->table->currentRow() >= 0) {
        new InvoiceView(invoices->at(ui->table->currentRow()), this);
    }
}


void InvoiceHistory::onCellDoubleClicked(int row, int column) {
    Q_UNUSED(column);
    parent->invoiceRelay(invoices->at(row));
}


void InvoiceHistory::onLoadButtonClicked() {
    if(ui->table->currentRow() >= 0) {
        parent->invoiceRelay(invoices->at(ui->table->currentRow()));
    }
}


void InvoiceHistory::deleteInvoice() {
    if(ui->table->currentRow() >= 0) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this,
                                      "Usunąć produkty?",
                                      "Czy usunąć też wszystkie pozwiązane produkty?",
                                      QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
        bool remove = (reply == QMessageBox::Yes);

        if (reply != QMessageBox::Cancel) {
            dbO->deleteInvoice(invoices->at(ui->table->currentRow()), remove);
            ui->table->removeRow(ui->table->currentRow());
            invoices->removeAt(ui->table->currentColumn());
        }
    }
}


InvoiceHistory::~InvoiceHistory() {
    delete ui;
}
