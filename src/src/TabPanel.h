#ifndef TABPANEL_H
#define TABPANEL_H

#include "InvoiceTab.h"
#include "firstseller.h"
#include "SettingsTab.h"
#include "productshistory.h"
#include "DatabaseOperator.h"
#include "invoicehistory.h"
#include "data/style.h"

#include <QString>
#include <QWidget>
#include <QTabWidget>
#include <QStringList>
#include <QVBoxLayout>
#include <QMainWindow>

class QTabWidget;
class QDialogButtonBox;
class InvoiceHistory;
class ProductsHistory;

/**
 * @brief The main window containing tabs where user swiches between views.
*/
class TabPanel : public QMainWindow {
        Q_OBJECT

    public:
        TabPanel(QWidget *parent = 0);
        DatabaseOperator *dbo;
        void invoiceRelay(Invoice *invoice);
        void itemRelay(Item *item);

    private:
        QTabWidget *tabWidget; ///< Allows to display tabs in window
        InvoiceTab *invoiceTab; ///< invoiceTab.cpp
        SettingsTab *settingsTab; ///< settingsTab.cpp
        InvoiceHistory *invoiceHistoryTab;
        ProductsHistory *productshistory;

        QVBoxLayout *mainLayout;

        /**
        * @brief Created directory if it doesn't exist.
        * @param dir Directory to check and created.
        */
        void createDirIfNotExists();

};

#endif
