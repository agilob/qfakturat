#ifndef SETTINGSTAB_H
#define SETTINGSTAB_H

#include "DatabaseOperator.h"
#include "sellerform.h"
#include "databasesettings.h"

#include <QLabel>
#include <QObject>
#include <QWidget>
#include <QFileInfo>
#include <QGroupBox>
#include <QCheckBox>
#include <QGroupBox>
#include <QListWidget>
#include <QGridLayout>
#include <QTreeWidget>
#include <QVBoxLayout>

/**
 * @brief A tab with buttons where user can select which setting are displayed.
 */
class SettingsTab : public QWidget {
        Q_OBJECT

    public:
        /**
         * @param parent Required by Qt
         */
        SettingsTab(QWidget *parent, DatabaseOperator *pdb);

    public slots:
        /**
         * @brief Display database's settings.
         */
        void changeWidgetToDatabase();
        /**
         * @brief Display seller's settings.
         */
        void changeWidgetToSellerSettings();

    private:

        QWidget *field; ///< A filed will be reloaded after selecting settings
        DatabaseSettings *databaseSettings; ///< A fileld where user selects how application behaves
        SellerForm *sf;

        QGroupBox *leftMenuBox; ///< Box with buttons to operate on settings tabs
        QVBoxLayout *leftMenuLayout; ///< Vertical layout of buttons
        QGridLayout *gridLayout; ///< Grid for setting on right hand side

        QPushButton *sellerSettingsButton, ///< After push form with sellers info will be displayed
                    *databaseSettingsButton; ///< After push form with application setting will be displayed

};

#endif  /* SETTINGSTAB_H */
