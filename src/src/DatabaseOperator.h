#ifndef DATABASEOPERATOR_H
#define DATABASEOPERATOR_H

#include "data/Invoice.h"
#include "data/Settings.h"

#include <QDir>
#include <QList>
#include <QDebug>
#include <QString>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlResult>
#include <QSqlDatabase>

/**
 * @brief The DatabaseOperator class contains functions to load and save data in local database.
 */
class DatabaseOperator {
    public:

        /**
         * @brief Creates main (and only) database for QFakturat.
         *
         * @return  true or false, depends if database was created
         * successfully.
         */
        void createDatabase();

        /**
         * @brief Method will create a table in main BD for information about
         * sellers.
         *
         * @return true or false, depends on result of creating table
         */
        void createTableSeller();

        /**
         * @brief Checks if there is any seller in the database. Method used to find out if user needs to add a seller.
         * @return True if there is a seller, false if no seller is in the database.
         */
        bool defaultSellerExists();

        QList<Item *> *loadItems();

        /**
         * @brief Method will create a table in main BD for information about
         * clients.
         *
         * @return true or false, depends on result of creating table
         */
        void createTableClients();

        void createInvoiceTable();

        void createItemTable();

        /**
         * @brief Closes connection to database.
         *
         * Closes connection to database, actually this method is never called
         * until the destructor is not called.
         */
        void closeDatabase();

        void saveItems(QList<Item *> *items, QString invoice);

        /**
         * @brief Get from database first default seller.
         * Method is called from invoiceTab after generation is allowed.
         */
        Seller *getDefaultSeller();

        QList<Item *> *getItems(QString invoice);

        /**
         * @brief Gets a list of clients from database to show them in a table.
         * Method is called from clientSelector
         */
        QList<Client *> *getClients();

        void deleteSeller(Seller *seller);

        void saveSeller(Seller *seller);

        QStringList sellersIDs();

        static QString getPath();

        void deleteInvoice(Invoice *invoice, bool deleteItems);

        void deleteProducts(Invoice *invoice);

        QList<Invoice *> *last100();

        Seller *getSeller(Seller *seller);

        Client *getClient(QString nip);

        Client *getClient(Client *client);

        /**
        * @brief Save invoice to the database, as a backup.
        */
        void saveInvoice(Invoice *invoice);

        void saveClient(Client *client);

        void debugQuery();

        QString lastInvoiceId();

        DatabaseOperator();
        DatabaseOperator(QString name);
        DatabaseOperator(const DatabaseOperator &orig);
        virtual ~DatabaseOperator();

    private:
        QSqlDatabase db; ///< object of database
        QSqlQuery query; ///< allows us query the database, used in global
        Seller *seller;
        Client *client;
        QList<Client *> *listOfClients;
        void alterTables();
        void alterTableInvoice();
        static const QString DATABASE_FILENAME;
};

#endif  /* DATABASEOPERATOR_H */
