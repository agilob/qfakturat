#include "InvoiceTab.h"

InvoiceTab::InvoiceTab(QWidget *parent, DatabaseOperator *pdb) : QWidget(parent) {
    wholeLayout = new QGridLayout(); //left section and central section together
    leftBox = new QGroupBox("Parametry faktury"); //a box for all sections at left
    leftLayout = new QVBoxLayout();
    invoiceTypes = new QComboBox();
    invoice = new Invoice();
    dbO = pdb;

    foreach (QString type, Settings::getInstance().getInvoiceTypes().split(",")) {
        if(type.startsWith(' ')) {
            type = type.remove(0, 1);
        }

        invoiceTypes->addItem(type);
    }

    invoiceTypesBox = new QGroupBox("Rodzaj faktury");
    hbox = new QHBoxLayout();
    hbox->addWidget(invoiceTypes);
    hbox->addStretch(1);
    invoiceTypesBox->setLayout(hbox);
    leftLayout->addWidget(invoiceTypesBox);
    i_d = new InvoiceDetails(this);
    leftLayout->addWidget(i_d->groupBox(), 1, Qt::AlignTop);
    p_d = new PaymentDetails(this);
    leftLayout->addWidget(p_d->groupBox(), 1, Qt::AlignTop);
    Seller *seller = dbO->getDefaultSeller();
    i_d->setCity(seller->city);
    extraSettings = nullptr;

    if(Settings::getInstance().getAutoInvoiceNumber()) {
        i_d->setInvoiceID(getLastInvoiceID(dbO->lastInvoiceId()));
    }

    /*
     * Form with buyer details and list of products.
     *
     */
    centralBox = new QGroupBox(QString::fromUtf8("Dane kontrahenta i produktów"));
    centralLayout = new QGridLayout();
    /**
     * Information about buyer.
     *
     */
    buyerBox = new QGroupBox(QString::fromUtf8("Informacje o kupującym"));
    buyerLayout = new QGridLayout();
    nameLa = new QLabel("Nazwa firmy");
    nameSurnameLa = new QLabel(QString::fromUtf8("Imię i nazwisko"));
    addressLa = new QLabel(QString::fromUtf8("Adres"));
    streetLa = new QLabel(QString::fromUtf8("Ulica"));
    nipLa = new QLabel(QString::fromUtf8("NIP"));
    nameLE = new QLineEdit();
    nameSurnameLE = new QLineEdit;
    addressLE = new QLineEdit();
    streetLE = new QLineEdit();
    nipLE = new QLineEdit();
    buyerLayout->addWidget(nameLa, 0, 0, Qt::AlignTop);
    buyerLayout->addWidget(nameSurnameLa, 1, 0, Qt::AlignTop);
    buyerLayout->addWidget(addressLa, 2, 0, Qt::AlignTop);
    buyerLayout->addWidget(streetLa, 3, 0, Qt::AlignTop);
    buyerLayout->addWidget(nipLa, 4, 0, Qt::AlignTop);
    buyerLayout->addWidget(nameLE, 0, 1, Qt::AlignTop);
    buyerLayout->addWidget(nameSurnameLE, 1, 1, Qt::AlignTop);
    buyerLayout->addWidget(addressLE, 2, 1, Qt::AlignTop);
    buyerLayout->addWidget(streetLE, 3, 1, Qt::AlignTop);
    buyerLayout->addWidget(nipLE, 4, 1, Qt::AlignTop);
    buyerBox->setLayout(buyerLayout);
    centralLayout->addWidget(buyerBox, 0, 0, Qt::AlignTop);
    /**
     * Right-side box with buttons to manage the form.
     *
     */
    buttonsBox = new QGroupBox(QString::fromUtf8("Zarządzaj"));
    buttonsLayout = new QGridLayout();
    loadClientsButton = new QPushButton(QString::fromUtf8("&Wczytaj klienta"));
    saveClientButton = new QPushButton(QString::fromUtf8("Zapi&sz klienta"));
    removeRowButton = new QPushButton(QString::fromUtf8("&Usuń produkt"));
    addRowButton = new QPushButton(QString::fromUtf8("&Dodaj produkt"));
    additionalSettings = new QPushButton(QString::fromUtf8("Opcje dodatkowe"));
    acceptInvoiceButton = new QPushButton(QString::fromUtf8("&Wystaw\nfakturę"));
    QComboBox *currenciesCB = new QComboBox();
    QStringList currencies = Settings::getInstance().getCurrencies().split(",");
    currenciesCB->addItems(currencies);
    buttonsLayout->addWidget(loadClientsButton, 0, 0);
    buttonsLayout->addWidget(saveClientButton, 0, 1);
    buttonsLayout->addWidget(addRowButton, 1, 0);
    buttonsLayout->addWidget(removeRowButton, 1, 1);
    buttonsLayout->addWidget(additionalSettings, 3, 0, 3, 0);
    buttonsLayout->addWidget(acceptInvoiceButton, 6, 0, 6, 0);
    buttonsBox->setLayout(buttonsLayout);
    centralLayout->addWidget(buttonsBox, 0, 1, Qt::AlignRight | Qt::AlignTop);
    /**
     * Table with products.
     */
    itemsBox = new QGroupBox(QString::fromUtf8("Lista sprzedanych produktów"));
    itemsLayout = new QGridLayout();
    table = new ItemsTable(this);

    foreach (QString type, Settings::getInstance().getPaymentMethods().split(",")) {
        if(type.startsWith(' ')) {
            type = type.remove(0, 1);
        }

        methodList.append(type);
    }

    methodList.removeDuplicates();
    p_d->setPaymentMethods(methodList);
    //
    clients = dbO->getClients();
    //
    addAutocompleters();
    //
    itemsLayout->addWidget(table);
    itemsBox->setLayout(itemsLayout);
    centralLayout->addWidget(itemsBox, 1, 0, 1, 0);
    //
    centralBox->setLayout(centralLayout);
    leftBox->setLayout(leftLayout);
    wholeLayout->addWidget(leftBox, 0, 0, Qt::AlignLeft | Qt::AlignTop);
    wholeLayout->addWidget(centralBox, 0, 1, 1, 1);
    setLayout(wholeLayout);
    connect(nipLE,               &QLineEdit::editingFinished, this,  &InvoiceTab::validateNIP);
    connect(loadClientsButton,   &QPushButton::clicked,       this,  &InvoiceTab::openClientsWindow);
    connect(saveClientButton,    &QPushButton::clicked,       this,  &InvoiceTab::saveClient);
    connect(addRowButton,        &QPushButton::clicked,       table, &ItemsTable::addRow);
    connect(removeRowButton,     &QPushButton::clicked,       table, &ItemsTable::deleteRow);
    connect(additionalSettings,  &QPushButton::clicked,       this,  &InvoiceTab::onAdditionalSettings);
    connect(acceptInvoiceButton, &QPushButton::clicked,       this,  &InvoiceTab::acceptInvoice);
} //constructor


void InvoiceTab::addAutocompleters() {
    QStringList completers;
    QCompleter *qcompleter;

    // autocomplete by nip
    for(int i = 0; i< clients->size(); i++) {
        completers << clients->at(i)->NIP;
    }

    qcompleter = new QCompleter(completers, this);
    qcompleter->setCaseSensitivity(Qt::CaseInsensitive);
    nipLE->setCompleter(qcompleter);
    connect(qcompleter,
            static_cast<void (QCompleter::*)(const QString &)>(&QCompleter::activated),
            this, &InvoiceTab::findClientByNip);
    completers.clear();

    // autocomplete by company name
    for(int i = 0; i< clients->size(); i++) {
        completers << clients->at(i)->company;
    }

    qcompleter = new QCompleter(completers, this);
    qcompleter->setCaseSensitivity(Qt::CaseInsensitive);
    nameLE->setCompleter(qcompleter);
    connect(qcompleter,
            static_cast<void (QCompleter::*)(const QString &)>(&QCompleter::activated),
            this, &InvoiceTab::findClientByNip);
    completers.clear();

    // autocomplete by company owner
    for(int i = 0; i< clients->size(); i++) {
        completers << clients->at(i)->name;
    }

    qcompleter = new QCompleter(completers, this);
    qcompleter->setCaseSensitivity(Qt::CaseInsensitive);
    nameSurnameLE->setCompleter(qcompleter);
    connect(qcompleter,
            static_cast<void (QCompleter::*)(const QString &)>(&QCompleter::activated),
            this, &InvoiceTab::findClientByNip);
    completers.clear();

    // autocomplete by address
    for(int i = 0; i< clients->size(); i++) {
        completers << clients->at(i)->city;
    }

    qcompleter = new QCompleter(completers, this);
    qcompleter->setCaseSensitivity(Qt::CaseInsensitive);
    addressLE->setCompleter(qcompleter);
    connect(qcompleter,
            static_cast<void (QCompleter::*)(const QString &)>(&QCompleter::activated),
            this, &InvoiceTab::findClientByNip);
    completers.clear();

    // autocomplete by street
    for(int i = 0; i< clients->size(); i++) {
        completers << clients->at(i)->street;
    }

    qcompleter = new QCompleter(completers, this);
    qcompleter->setCaseSensitivity(Qt::CaseInsensitive);
    streetLE->setCompleter(qcompleter);
    connect(qcompleter,
            static_cast<void (QCompleter::*)(const QString &)>(&QCompleter::activated),
            this, &InvoiceTab::findClientByNip);
    completers.clear();
}


void InvoiceTab::validateNIP() {
    QPalette *p = new QPalette();

    if (Validators::validateNIP(nipLE->text())) {
        if(Settings::getInstance().getFormatNip() == true) {
            nipLE->setText(Validators::formatNIP(nipLE->text().remove(QRegExp("\\D*"))));
        } else {
            nipLE->setText(nipLE->text());
        }

        p->setColor(QPalette::Text, QColor(Qt::black));
    } else {
        p->setColor(QPalette::Text, QColor(Qt::red));
    }

    nipLE->setPalette(*p);
} //validate NIP


void InvoiceTab::setInvoice(Invoice *invoice) {
    table->removeAllRows();
    table->insertItems(invoice->items);
    setClient(invoice->client);
    validateNIP();
    i_d->setCity(invoice->location);
    i_d->setSellingDate(invoice->sellingDate);
    i_d->setProductionDate(invoice->creatingDate);
    p_d->setDeadlineDate(invoice->deadDate);
    i_d->setInvoiceID(invoice->invoiceNumber);
    p_d->setAlreadyPaid(invoice->paid);
    p_d->setToPay(invoice->toPay);
}


void InvoiceTab::addItem(Item *item) {
    table->insertItem(-1, item);
}


void InvoiceTab::setClient(Client *client) {
    nameLE->setText(client->company);
    nameSurnameLE->setText(client->name);
    nipLE->setText(client->NIP);
    addressLE->setText(client->city);
    streetLE->setText(client->street);
}


void InvoiceTab::findClientByNip(QString nip) {
    foreach (Client *client, *clients) {
        if(client->NIP == nip) {
            setClient(client);
        }
    }
}


void InvoiceTab::findClientByCompany(QString company) {
    foreach (Client *client, *clients) {
        if(client->company == company) {
            setClient(client);
        }
    }
}

void InvoiceTab::findClientByName(QString name) {
    foreach (Client *client, *clients) {
        if(client->name == name) {
            setClient(client);
        }
    }
}


void InvoiceTab::findClientByAddress(QString address) {
    foreach (Client *client, *clients) {
        if(client->city == address) {
            setClient(client);
        }
    }
}


void InvoiceTab::findClientByStreet(QString street) {
    foreach (Client *client, *clients) {
        if(client->street == street) {
            setClient(client);
        }
    }
}


void InvoiceTab::acceptInvoice() {
    invoice = new Invoice();
    Seller *seller = dbO->getDefaultSeller();
    invoice->type = invoiceTypes->currentText();

    if (nameLE->text() != "") {
        invoice->client->company = (nameLE->text());
    }

    invoice->seller = seller;
    invoice->client->name = nameSurnameLE->text();
    invoice->client->city = addressLE->text();
    invoice->client->street = streetLE->text();
    invoice->client->NIP = nipLE->text();
    invoice->creatingDate = QDate::currentDate();
    invoice->items->append(*table->getAllItems());
    invoice->sellingDate = i_d->productionDate();
    invoice->invoiceNumber = i_d->invoiceID();
    invoice->location = i_d->city();
    invoice->deadDate = p_d->deadlineDate();
    invoice->paid = p_d->alreadyPaid();
    invoice->toPay = p_d->toPay();
    invoice->payingMethod = p_d->paymentMethod();
    invoice->netto = sumUpToPayNetto();
    invoice->brutto = sumUpToPay();
    invoice->totalVat = invoice->brutto - invoice->netto;

    if(extraSettings == nullptr) {
        invoice->currency = Settings::getInstance().getCurrencies().split(",").at(0);
    } else {
        invoice->currency = extraSettings->getCurrency();
    }

    if(isSumValid() == true) {
        dbO->saveInvoice(invoice);
        //first save, then generate, if generation
        //fails, data can be recovered
        new Generator(invoice);
    }
} //acceptInvoice


QString InvoiceTab::getLastInvoiceID(QString lastInvoiceId) {
    QString curMonth = QString::number(QDate::currentDate().month());
    QRegularExpression idParser("[0-9]*");
    QRegularExpression monthParser("/[0-9]*/");
    QRegularExpression monthYearParser("/[0-9]*$");
    QString lastId = idParser.match(lastInvoiceId).captured(0);
    QString lastMonth = monthParser.match(lastInvoiceId).captured(0).replace("/", "");
    QString monthYear = monthYearParser.match(lastInvoiceId).captured(0);

    if(curMonth.toInt() == lastMonth.toInt()) {
        lastId[lastId.size() - 1].unicode()++;
    } else {
        lastId = "01";
    }

    if(QDate::currentDate().month() < 10) {
        curMonth = "0" + curMonth;
    }

    if(monthYear.isEmpty() || monthYear.length() == 0) {
        monthYear = "/" + QString::number(QDate::currentDate().year());
    }

    lastId += "/" + curMonth + monthYear;
    return lastId;
}

double InvoiceTab::sumUpToPay() {
    return table->sumUpGross();
}


double InvoiceTab::sumUpToPayNetto() {
    return table->sumUpNet();
}

bool InvoiceTab::isSumValid() {
    double totalSum = p_d->alreadyPaid() + p_d->toPay();
    double sumProducts = table->sumUpGross();

    if(totalSum != sumProducts) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Niepoprawna suma cen brutto"));
        msgBox.setText("Suma kwot do zapłaty i zapłacono jest różna od sum cen brutto produków!\n"
                       "Suma cen brutto produktów: " + QString::number(sumProducts, 'f', 2) +
                       "\nSuma do zapłaty i zapłacono: " + QString::number(totalSum, 'f', 2) +
                       "\nCzy chcesz kontynuować?");
        msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        msgBox.setIcon(QMessageBox::Warning);
        return (msgBox.exec() == QMessageBox::Yes);
    }

    return true;
}


void InvoiceTab::setToPay(double toPay) {
    if(Settings::getInstance().getIsToPayDefault() == true) {
        p_d->setToPay(toPay);
    } else {
        p_d->setAlreadyPaid(toPay);
    }
}


Client *InvoiceTab::getClient() {
    Client *client = new Client();
    client->name = nameLE->text();
    client->company = nameSurnameLE->text();
    client->city = addressLE->text();
    client->street = streetLE->text();
    client->NIP = nipLE->text();
    return client;
}


void InvoiceTab::openClientsWindow() {
    clients->clear();
    clients = dbO->getClients();
    clientsLoader = new ClientsSelector(this, clients);
}


void InvoiceTab::onAdditionalSettings() {
    extraSettings = new ExtraSettings(this);
}


void InvoiceTab::loadClientFromRow(int row, int column) {
    Q_UNUSED(column);
    setClient(clients->at(row));
    clientsLoader->close();
}


void InvoiceTab::loadClient() {
    int row = clientsLoader->table->currentRow();
    setClient(clients->at(row));
    clientsLoader->close();
}


void InvoiceTab::saveClient() {
    dbO->saveClient(getClient());
}


void InvoiceTab::loadDefaultSeller() {
    invoice->seller = dbO->getDefaultSeller();
}
