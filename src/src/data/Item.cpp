#include "data/Item.h"

Item::Item() {}

QString Item::getName() const {
    return name;
}

void Item::setName(const QString &value) {
    name = value;
}
QString Item::getMeasure() const {
    return measure;
}

void Item::setMeasure(const QString &value) {
    measure = value;
}

double Item::getQuantity() const {
    return quantity;
}

void Item::setQuantity(double value) {
    quantity = value;
}
int Item::getVat() const {
    return vat;
}

void Item::setVat(int value) {
    vat = value;
}

double Item::getNetto() const {
    return netto;
}

void Item::setNetto(double value) {
    netto = value;
}
double Item::getGross() const {
    return gross;
}

void Item::setGross(double value) {
    gross = value;
}
double Item::getSinglePrice() const {
    return singlePrice;
}

void Item::setSinglePrice(double value) {
    singlePrice = value;
}
