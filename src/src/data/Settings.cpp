#include "data/Settings.h"

const QString Settings::FILENAME = "/qfakturat.ini";
const QString Settings::DIRNAME = "/qfakturat/";

Settings *Settings::settings{nullptr};

void Settings::load() {
    if (loaded) {
        return;
    }

    QString filePath = getSettingsFilePath();

    //if no settings file exist -- use the default one
    if (!QFile(getSettingsFilePath()).exists()) {
        if (!QFile(getSettingsFilePath()).exists()) {
            qDebug() << "No settings file found, using defaults";
            filePath = ":/res/" + FILENAME;
        }
    }

    qDebug() << "loading: " << filePath;

    if (QFile(filePath).exists()) {
        QSettings s(filePath, QSettings::IniFormat);
        s.beginGroup("General");  //ui and backend
        isToPayDefault = s.value("isToPayDefault", false).toBool();
        statisticDataEnabled = s.value("statisticDataEnabled", true).toBool();
        autoInvoiceNumber = s.value("autoInvoiceNumber", true).toBool();
        useSystemTheme = s.value("useSystemTheme", false).toBool();
        theme = s.value("theme", 0).toInt();
        s.endGroup();
        s.beginGroup("Invoice"); //behaviour and values
        taxes = s.value("taxes", "0, 5, 7, 8, 22, 23").toString();
        measures = s.value("measures", "szt., kg, m, litr, m², m³").toString();
        paymentMethods = s.value("paymentMethods", "Przelew, Gotówka").toString();
        invoiceTypes = s.value("invoiceTypes", "Faktura VAT, Korekta faktury VAT, Proforma").toString();
        defaultVAT = s.value("defaultVAT", 23).toString();
        currencies = s.value("currencies", "PLN, EURO, GBP").toString();
        //isValidatonOn = s.value("isValidatonOn", true).toBool();
        daysForPayment = s.value("daysForPayment", 7).toInt();
        formatNip = s.value("formatNip", true).toBool();
        s.endGroup();
        loaded = true;
    } else {
        save();
    }
}

void Settings::save() {
    qDebug() << "Settings: Saving in" << getSettingsFilePath();
    QSettings s(getSettingsFilePath(), QSettings::IniFormat);
    s.clear();
    s.beginGroup("General");
    s.setValue("isToPayDefault", isToPayDefault);
    s.setValue("statisticDataEnabled", statisticDataEnabled);
    s.setValue("autoInvoiceNumber", autoInvoiceNumber);
    s.setValue("useSystemTheme", useSystemTheme);
    s.setValue("theme", theme);
    s.endGroup();
    s.beginGroup("Invoice");
    s.setValue("taxes", taxes);
    s.setValue("measures", measures);
    s.setValue("paymentMethods", paymentMethods);
    s.setValue("invoiceTypes", invoiceTypes);
    s.setValue("defaultVAT", defaultVAT);
    s.setValue("currencies", currencies);
    //s.setValue("isValidatonOn", isValidatonOn);
    s.setValue("daysForPayment", daysForPayment);
    s.setValue("formatNip", formatNip);
    s.endGroup();
}

QString Settings::getSettingsFilePath() {
    return Settings::getSettingsDirPath() + FILENAME;
}

QString Settings::getSettingsDirPath() {
    // workaround for https://bugreports.qt-project.org/browse/QTBUG-38845
#ifdef Q_OS_WIN
    return QDir::cleanPath(QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +
                           QDir::separator() + "AppData" + QDir::separator() +
                           "Roaming" + QDir::separator() + DIRNAME);
#else
    return QDir::cleanPath(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
                           QDir::separator() + DIRNAME);
#endif
}

Settings &Settings::getInstance() {
    if (!settings) {
        settings = new Settings();
    }

    return *settings;
}

Settings::Settings() : loaded(false) {
    load();
}
QStringList Settings::getThemes() const {
    return themes;
}

void Settings::setThemes(const QStringList &value) {
    themes = value;
}

int Settings::getTheme() const {
    return theme;
}

void Settings::setTheme(int value) {
    theme = value;
}

bool Settings::getUseSystemTheme() const {
    return useSystemTheme;
}

void Settings::setUseSystemTheme(bool value) {
    useSystemTheme = value;
}

bool Settings::getAutoInvoiceNumber() const {
    return autoInvoiceNumber;
}

void Settings::setAutoInvoiceNumber(bool value) {
    autoInvoiceNumber = value;
}

bool Settings::getFormatNip() const {
    return formatNip;
}

void Settings::setFormatNip(bool value) {
    formatNip = value;
}

int Settings::getDaysForPayment() const {
    return daysForPayment;
}

void Settings::setDaysForPayment(int value) {
    daysForPayment = value;
}

//bool Settings::getIsValidatonOn() const
//{
//    return isValidatonOn;
//}

//void Settings::setIsValidatonOn(bool value)
//{
//    isValidatonOn = value;
//}

QString Settings::getCurrencies() const {
    return currencies;
}

void Settings::setCurrencies(const QString &value) {
    currencies = value;
}

bool Settings::getStatisticDataEnabled() const {
    return statisticDataEnabled;
}

void Settings::setStatisticDataEnabled(bool value) {
    statisticDataEnabled = value;
}

bool Settings::getFirstTime() const {
    return firstTime;
}

void Settings::setFirstTime(bool value) {
    firstTime = value;
}

QString Settings::getDefaultVAT() const {
    return defaultVAT;
}

void Settings::setDefaultVAT(const QString &value) {
    defaultVAT = value;
}

QString Settings::getTaxes() const {
    return taxes;
}

void Settings::setTaxes(const QString &value) {
    taxes = value;
}

QString Settings::getMeasures() const {
    return measures;
}

void Settings::setMeasures(const QString &value) {
    measures = value;
}

QString Settings::getCity() const {
    return city;
}

void Settings::setCity(const QString &value) {
    city = value;
}

QString Settings::getInvoiceTypes() const {
    return invoiceTypes;
}

void Settings::setInvoiceTypes(const QString &value) {
    invoiceTypes = value;
}

QString Settings::getPaymentMethods() const {
    return paymentMethods;
}

void Settings::setPaymentMethods(const QString &value) {
    paymentMethods = value;
}

bool Settings::getIsToPayDefault() const {
    return isToPayDefault;
}

void Settings::setIsToPayDefault(bool value) {
    isToPayDefault = value;
}
