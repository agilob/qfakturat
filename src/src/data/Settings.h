#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QFile>
#include <QSettings>
#include <QDir>
#include <QDebug>
#include <QStandardPaths>
#include <QApplication>
#include <QStringList>

class Settings : public QObject {
        Q_OBJECT

    public:
        static Settings &getInstance();
        ~Settings() = default;
        static QString getSettingsDirPath(); ///< provides absolute path to config folder
        static QString getSettingsFilePath(); ///< provides location of qfakturat.ini

        void load();
        void save();

        bool getFirstTime() const;
        void setFirstTime(bool value);

        int getSetFirstTab() const;
        void setSetFirstTab(int value);

        QString getDefaultVAT() const;
        void setDefaultVAT(const QString &value);

        QString getTaxes() const;
        void setTaxes(const QString &value);

        QString getMeasures() const;
        void setMeasures(const QString &value);

        QString getCity() const;
        void setCity(const QString &value);

        QString getInvoiceTypes() const;
        void setInvoiceTypes(const QString &value);

        QString getPaymentMethods() const;
        void setPaymentMethods(const QString &value);

        bool getIsToPayDefault() const;
        void setIsToPayDefault(bool value);

        bool getStatisticDataEnabled() const;
        void setStatisticDataEnabled(bool value);

        QString getCurrencies() const;
        void setCurrencies(const QString &value);

//    bool getIsValidatonOn() const;
//    void setIsValidatonOn(bool value);

        int getDaysForPayment() const;
        void setDaysForPayment(int value);

        bool getFormatNip() const;
        void setFormatNip(bool value);

        bool getAutoInvoiceNumber() const;
        void setAutoInvoiceNumber(bool value);

        bool getUseSystemTheme() const;
        void setUseSystemTheme(bool value);

        int getTheme() const;
        void setTheme(int value);

        QStringList getThemes() const;
        void setThemes(const QStringList &value);

    private:
        static Settings *settings;

        Settings();
        Settings(Settings &settings) = delete;
        Settings &operator=(const Settings &) = delete;

        bool loaded; ///< are settings loaded
        static const QString FILENAME; ///< name of the config file
        static const QString DIRNAME; ///< folder name where configs are

        bool firstTime = true; ///< is it first time we run the app?
        bool statisticDataEnabled = true; ///< true = allow collection
        bool isToPayDefault = true; ///< default field where the price will be added
//unused?    //bool isValidatonOn = true; ///< Polish NIP validation, turn off for forein currencies
        bool formatNip = false; ///< insert `-` between numbers in NIP to make it easier to read
        bool autoInvoiceNumber = true;
        bool useSystemTheme = false;

        int daysForPayment = 7; ///< default number of days added to current date to set deadline date
        int theme = 0; //light by default
        QStringList themes = (QStringList() << "light" << "darkblue" << "wombat"); // names of css files in /res/
 // names of css files in /res/

        QString defaultVAT = "23"; ///< default tax
        QString taxes = "0, 5, 7, 8, 22, 23"; ///< a list of taxes
        QString measures = "szt., kg, m, litr, m², m³"; ///< a list of items measures like l. kg. m^2
        QString city = "Miasto"; ///< where the invoice is generated, or something like this
        QString invoiceTypes = "Faktura VAT, Korekta faktury VAT, Proforma"; ///< type of the invoice, original, proforma, fix
        QString paymentMethods = "Przelew, Gotówka"; ///< cash, bank transfer
        QString currencies = "PLN, EURO, GBP"; ///< default currencies
};

#endif  /* SETTINGS_H */
