#ifndef ITEM_H
#define ITEM_H

#include <QString>

class Item {

    public:
        Item();

        double getPrice() const;
        void setPrice(double value);

        QString getName() const;
        void setName(const QString &value);

        QString getMeasure() const;
        void setMeasure(const QString &value);

        double getQuantity() const;
        void setQuantity(double value);

        int getVat() const;
        void setVat(int value);

        double getNetto() const;
        void setNetto(double value);

        double getGross() const;
        void setGross(double value);

        double getSinglePrice() const;
        void setSinglePrice(double value);

    private:
        QString name;
        QString measure; ///< measure: meters, kilos, liters etc.

        double singlePrice; ///< price for one product
        double price; ///< total price for items
        double netto; ///< price netto for all products
        double gross; ///< price brutto for all products
        double quantity; ///< quantity of products
        int vat; ///< vat for item

};

#endif  /* ITEM_H */

