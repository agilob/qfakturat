#ifndef CLIENT_H
#define CLIENT_H

#include <QString>

class Client {

    public:

        Client();
        virtual ~Client();

        QString company,
                name,
                city,
                street,
                NIP;

};

#endif  /* CLIENT_H */

