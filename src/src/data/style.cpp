#include "style.h"

QString Style::getStylesheet(const QString &filename) {
    if (!Settings::getInstance().getUseSystemTheme()) {
        QFile file(filename);

        if (file.open(QFile::ReadOnly | QFile::Text)) {
            return file.readAll();
        } else {
            qWarning() << "Style: Stylesheet" << filename << "not found";
        }
    }

    return QString();
}
