#ifndef STYLE_H
#define STYLE_H

#include <QFile>
#include <QDebug>

#include "data/Settings.h"

class Style {
    public:
        static QString getStylesheet(const QString &filename);

    private:
        Style();
};

#endif // STYLE_H
