#ifndef SELLER_H
#define SELLER_H

#include <QString>

class Seller {

    public:

        Seller();
        Seller(const Seller &orig);
        virtual ~Seller();

        QString id,
                company,
                name,
                postcode,
                city,
                street,
                NIP,
                www,
                email,
                phone,
                bankAccount,
                rightText,
                bottomText;

        bool isDefault;

        int size();

};

#endif  /* SELLER_H */


