#ifndef INVOICE_H
#define INVOICE_H

#include <QDate>
#include <QString>

#include "Item.h"
#include "Seller.h"
#include "Client.h"

class Invoice {

    public:
        Invoice();
        Invoice(const Invoice &orig);
        virtual ~Invoice();

        Seller *seller; ///< A list with detail about seller
        Client *client; ///< A list with detail about buyer

        QString invoiceNumber, ///< Invoice number for this time
                location, ///< City for the invoice
                payingMethod, ///< cash, bank transfer etc.
                type,
                info,
                currency;

        QDate sellingDate, ///< When user sold products
              deadDate, ///< Deadline of paying
              creatingDate; ///< Date when invoice is officialy produced


        QList<Item *> *items; ///< List of products

        double paid, ///< How much is already paid.
               toPay, ///< How much is to pay for the invoice.
               netto, ///< Netto value to pay.
               brutto, ///< Brutto value to pay.
               totalVat; ///< total tax

};

#endif  /* INVOICE_H */
