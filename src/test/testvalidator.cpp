#include <QtTest/QtTest>
#include "../src/Validators.h"

class ValidatorTest: public QObject
{
    Q_OBJECT

private slots:
    void testValidNip();
    void testNipFormatter();
};

void ValidatorTest::testValidNip()
{
    /**
     * Generally valid when special chars are removed, so it's ok
    */
    QVERIFY(Validators::validateNIP("772-109-10-48"));
    QVERIFY(Validators::validateNIP("9999999999"));

    QVERIFY(Validators::validateNIP("772.109.10.48"));
    QVERIFY(Validators::validateNIP("PL772-109-10-48"));

    // checksum not valid
    QVERIFY(!Validators::validateNIP("PL772-109-10-49"));

    // too short
    QVERIFY(!Validators::validateNIP("PL1234"));
}

void ValidatorTest::testNipFormatter() {
    QCOMPARE(Validators::formatNIP("9999999999"), QString("999-999-99-99"));
}

QTEST_MAIN(ValidatorTest)

#include "testvalidator.moc"
