#include "TabPanel.h"
#include "DatabaseOperator.h"
#include <QApplication>
#include <QDesktopWidget>


int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    TabPanel *tabdialog = new TabPanel();

    if(Settings::getInstance().getUseSystemTheme() == false) {
        app.setStyleSheet(
            Style::getStylesheet(":/css/" +
                                 Settings::getInstance().getThemes().at(Settings::getInstance().getTheme()) + ".css"));
    }

    tabdialog->show();
    tabdialog->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            tabdialog->size(),
            qApp->desktop()->availableGeometry()
        )
    );
    return app.exec();
}
