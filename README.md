# QFakturat
QFakturat jest desktopowym programem na Linux do wystawiania faktur, zarządzania produktami i fakturami oraz danymi klientów.

Pozwala na zapisywanie, usuwanie, edytowanie, filtrowanie
 * faktur
 * produktów
 * klientów
 
Możliwe jest wczytywanie i edytowanie poprzednio wystawionych faktur, wystawianie ich ponownie po modyfikacji.
Program ma duże możliwości dostosowania zachowania i danych do własnych potrzeb, pozwala ustawić użytkownikowi pola jak:
 * stawki VAT
 * domyślną stawkę VAT
 * rodzaje faktur (VAT, proforma itp.)
 * domyślnego sprzedawcę
 * waluty
 * czas na zapłatę
 * metody płatności
 * jednostki miar produktów
 * **dużo więcej**

Poniższe screeny wyjaśnią więcej niż jestem tu w stanie opisać (mogły ulec delikatnym zmianom).

Początowo program był pisany dla własnych potrzeb.

## Wygląd faktury
Wygląd faktury zależny jest od wypełnionych pól w ustawieniach sprzedawcy.
Puste pola nie zostaną wyświetlone na fakturze, [tutaj możesz zobaczyć przykładową fakturę jako .pdf](screenshots/przukladowa_faktura.pdf)

![qfakturat_przykladowa_fakturat](screenshots/faktura.png)

## Screenshoty

![qfakturat_formularz_faktur_light.png](screenshots/formularz_faktur_light.png)
![qfakturat_historia_produktow_light.png](screenshots/historia_produktow_light.png)
![qfakturat_ustawienia_sprzedawcy_light.png](screenshots/ustawienia_sprzedawcy_light.png)
![qfakturat_ustawienia_bazy_light.png](screenshots/ustawienia_bazy_light.png)
![qfakturat_ustawienia_bazy_darkblue.png](screenshots/ustawienia_bazy_darkblue.png)
![qfakturat_formularz_faktur_wombat.png](screenshots/formularz_faktur_wombat.png)
[![piwik](https://agilob.net/piwik/piwik.php?idsite=5&rec=1)](https://agilob.net/) 