######################################################################
# Automatically generated by qmake (3.0) Sun Mar 9 17:23:06 2014
######################################################################

QT += sql core gui widgets printsupport

QMAKE_CC = clang
QMAKE_CXX = clang

TEMPLATE = app

MOC_DIR = .moc
OBJECTS_DIR = .obj
TARGET = qfakturat

ICON = rec/icon.png

INCLUDEPATH += . src/headers src/src rsc
DEPENDPATH += . src src/src src/src/data

CONFIG += silent c++11

RESOURCES += res.qrc
# Input
HEADERS += src/src/ClientsSelector.h \
        src/src/DatabaseOperator.h \
        src/src/Generator.h \
        src/src/InvoiceTab.h \
        src/src/SettingsTab.h \
        src/src/TabPanel.h \
        src/src/Validators.h \
        src/src/data/Client.h \
        src/src/data/Invoice.h \
        src/src/data/Item.h \
        src/src/data/Seller.h \
        src/src/data/Settings.h \
        src/src/productshistory.h \
        src/src/firstseller.h \
        src/src/sellerform.h \
        src/src/invoiceWidgets/paymentDetails.h \
        src/src/invoiceWidgets/invoiceDetails.h \
        src/src/invoiceWidgets/itemstable.h \
        src/src/databasesettings.h \
        src/src/invoiceWidgets/extrasettings.h \
        src/src/invoiceview.h \
        src/src/invoicehistory.h \
        src/src/data/style.h

SOURCES += src/main.cpp \
        src/src/ClientsSelector.cpp \
        src/src/DatabaseOperator.cpp \
        src/src/Generator.cpp \
        src/src/InvoiceTab.cpp \
        src/src/SettingsTab.cpp \
        src/src/TabPanel.cpp \
        src/src/Validators.cpp \
        src/src/data/Client.cpp \
        src/src/data/Invoice.cpp \
        src/src/data/Item.cpp \
        src/src/data/Seller.cpp \
        src/src/data/Settings.cpp \
        src/src/productshistory.cpp \
        src/src/firstseller.cpp \
        src/src/sellerform.cpp \
        src/src/invoiceWidgets/paymentDetails.cpp \
        src/src/invoiceWidgets/invoiceDetails.cpp \
        src/src/invoiceWidgets/itemstable.cpp \
        src/src/databasesettings.cpp \
        src/src/invoiceWidgets/extrasettings.cpp \
        src/src/invoiceview.cpp \
        src/src/invoicehistory.cpp \
        src/src/data/style.cpp

FORMS += src/src/productshistory.ui \
        src/src/firstseller.ui \
        src/src/sellerform.ui \
        src/src/invoiceWidgets/paymentDetails.ui \
        src/src/invoiceWidgets/invoiceDetails.ui \
        src/src/databasesettings.ui \
        src/src/invoiceWidgets/extrasettings.ui \
        src/src/invoiceview.ui \
        src/src/invoicehistory.ui

OTHER_FILES += Makefile
